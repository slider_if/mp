/*global module:false*/
module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    // Metadata.
    pkg: grunt.file.readJSON('package.json'),
    banner: '/*! <%= pkg.title || pkg.name %> - v<%= pkg.version %> - ' +
      '<%= grunt.template.today("yyyy-mm-dd") %>\n' +
      '<%= pkg.homepage ? "* " + pkg.homepage + "\\n" : "" %>' +
      '* Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.author.name %>;' +
      ' Licensed <%= _.pluck(pkg.licenses, "type").join(", ") %> */\n',
    // Task configuration.
    concat: {
      options: {
        //banner: '<%= banner %>',
        //stripBanners: true
      },
      js: {
        src: ['web/bundles/dk/vendor/jquery/dist/*.min.js',
              'web/bundles/dk/vendor/bootstrap/dist/js/*.min.js',
              'web/bundles/dk/vendor/angular/*.min.js',
              'web/bundles/dk/vendor/select2/*.min.js',
              'web/bundles/dk/vendor/jquery.customSelect/jquery.customSelect.min.js',
              'web/bundles/dk/vendor/jqtree/*.js',
              ],
        dest: 'web/bundles/dk/js/libs.js'
      },
      css: {
        src: ['web/bundles/dk/vendor/bootstrap/dist/css/*.css',
              'web/bundles/dk/vendor/select2/*.css',
              'web/bundles/dk/vendor/jqtree/*.css'
              ],
        dest: 'web/bundles/dk/css/libs.css'
      }
    },
    copy: {
      main: {
          expand: true,
          flatten: true,
          src: [ 'web/bundles/dk/vendor/select2/*.png', 
                  'web/bundles/dk/vendor/select2/*.gif'
            ], 
          dest: 'web/bundles/dk/css/'
      },
      fonts: {
        expand: true,
        flatten: true,
        src: ['web/bundles/dk/vendor/bootstrap/fonts/*'],
        dest: 'web/bundles/dk/fonts/'
      }
    },
    compass: {
      dist: {
        options: {
          sassDir: 'web/bundles/dk/sass',
          cssDir: 'web/bundles/dk/css',
          basePath: ''
        }
      }
    },
    uglify: {
      options: {
        banner: '<%= banner %>'
      },
      dist: {
        src: '<%= concat.dist.dest %>',
        dest: 'dist/<%= pkg.name %>.min.js'
      }
    },
    jshint: {
      options: {
        curly: true,
        eqeqeq: true,
        immed: true,
        latedef: true,
        newcap: true,
        noarg: true,
        sub: true,
        undef: true,
        unused: true,
        boss: true,
        eqnull: true,
        browser: true,
        globals: {
          jQuery: true
        }
      },
      gruntfile: {
        src: 'Gruntfile.js'
      },
      lib_test: {
        src: ['lib/**/*.js', 'test/**/*.js']
      }
    },
    qunit: {
      files: ['test/**/*.html']
    },
    watch: {
      gruntfile: {
        files: '<%= jshint.gruntfile.src %>',
        tasks: ['jshint:gruntfile']
      },
      lib_test: {
        files: '<%= jshint.lib_test.src %>',
        tasks: ['jshint:lib_test', 'qunit']
      },
      scss: {
        files: 'web/bundles/dk/sass/*.scss',
        tasks: ['compass']
      },
    }
  });

  // These plugins provide necessary tasks.
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-qunit');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-compass');

  // Default task.
  grunt.registerTask('default', ['concat', 'copy', 'watch']);

};
