'use strict';

var app = angular.module('app', []);

app.controller('index', function ($scope, $http) {
  // $http.get('', {responseType: "json"}).success(function(data) {
  //   $scope.body = data.html;
  // });

  
});

$(document).ready(function() {

	$('#cat-tree').tree({
       data: catalgData
    });

    $('#cat-serv-tree').tree({
       data: catalgServData
    });

	$('#search-category').customSelect();

	function closeAll() {
		$('.button-with-popup .button-popup.active').removeClass('active');
		$('.button-with-popup button.active').removeClass('active');
	}

	$('.button-with-popup button').click(function(e) {
		closeAll();
		var item = $(this).parent();

		item.find('.button-popup').addClass('active');
		item.find('button').addClass('active');

		return false;
	});

	$('body').click(function() {
		closeAll();
	});

	 $("#timepicker").DateTimePicker({
	 	mode: "datetime",
	 	//dateSeparator: ".",
	 	//dateTimeFormat: "dd.MM.yyyy hh:mm",
	 	shortDayNames: ["Нд", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
	 	fullDayNames:  	["Неділя", "Понеділок", "Вівторок", "Середа", "Четвер", "П'ятниця", "Субота"],
	 	shortMonthNames: ["Січень", "Лютий", "Березень", "Квітень", "Травень", "Червень", "Липень", "Серпень", "Вересень", "Жовтень", "Листопад", "Грудень"],
	 	fullMonthNames: ["Січень", "Лютий", "Березень", "Квітень", "Травень", "Червень", "Липень", "Серпень", "Вересень", "Жовтень", "Листопад", "Грудень"],
	 	titleContentDate: "Встановити дату",
	 	titleContentTime: "Встановити час",
	 	setButtonContent: "Зберегти",
	 	clearButtonContent: 'Очистити',
	 	isPopup: true
	 });

	var addressSelectName = '';
	function setAddressSelect(target) {
		target.select2({ width: '200px' });

		target.change(function() {
			var item = $(this);
			var level = parseInt(item.data('level')) + 1;

			if(item.data('last')) {
				item.attr('name', addressSelectName);
				return;
			} else if(item.data('first') && item.attr('name').length > 0) {
				addressSelectName = item.attr('name');
				item.attr('data-name', addressSelectName);
				item.attr('name', '');
			}

			if(!item.val()) {
				$('.select2-container,.addressSelect').filter(function() {
					return ($(this).data('level') >= level)
				}).remove();
				return;
			}

			$.ajax({
				url: item.data('url') + item.val() + '/',
				success : function(data) {
					$('select.addressSelect').filter(function() {
						var localLevel = $(this).data('level');
						if(localLevel >= level) {
							$(this).parent().find('.select2-container.level-' + localLevel).remove();
							return true;
						}
						return false;
					}).remove();
					var html = $(data);
					html.addClass('level-' + level);
					html.attr('data-level', level);
					item.after(html);
					setAddressSelect(html);
				}
			});
		});
	}

	setAddressSelect($('.addressSelect'));
});