<?php

namespace MP\UserBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use MP\UserBundle\Entity\House;
use MP\UserBundle\Form\HouseType;

/**
 * House controller.
 *
 */
class HouseController extends Controller
{

    /**
     * Lists all House entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MPUserBundle:House')->findAll();

        return $this->render('MPUserBundle:House:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new House entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new House();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('house_show', array('id' => $entity->getId())));
        }

        return $this->render('MPUserBundle:House:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a House entity.
     *
     * @param House $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(House $entity)
    {
        $form = $this->createForm(new HouseType(), $entity, array(
            'action' => $this->generateUrl('house_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new House entity.
     *
     */
    public function newAction()
    {
        $entity = new House();
        $form   = $this->createCreateForm($entity);

        return $this->render('MPUserBundle:House:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a House entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MPUserBundle:House')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find House entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MPUserBundle:House:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing House entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MPUserBundle:House')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find House entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MPUserBundle:House:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a House entity.
    *
    * @param House $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(House $entity)
    {
        $form = $this->createForm(new HouseType(), $entity, array(
            'action' => $this->generateUrl('house_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing House entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MPUserBundle:House')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find House entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('house_edit', array('id' => $id)));
        }

        return $this->render('MPUserBundle:House:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a House entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('MPUserBundle:House')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find House entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('house'));
    }

    /**
     * Creates a form to delete a House entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('house_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
