<?php

namespace MP\UserBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use MP\UserBundle\Entity\Gender;
use MP\UserBundle\Form\GenderType;

/**
 * Gender controller.
 *
 */
class GenderController extends Controller
{

    /**
     * Lists all Gender entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MPUserBundle:Gender')->findAll();

        return $this->render('MPUserBundle:Gender:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Gender entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Gender();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('gender_show', array('id' => $entity->getId())));
        }

        return $this->render('MPUserBundle:Gender:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Gender entity.
     *
     * @param Gender $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Gender $entity)
    {
        $form = $this->createForm(new GenderType(), $entity, array(
            'action' => $this->generateUrl('gender_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Gender entity.
     *
     */
    public function newAction()
    {
        $entity = new Gender();
        $form   = $this->createCreateForm($entity);

        return $this->render('MPUserBundle:Gender:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Gender entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MPUserBundle:Gender')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Gender entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MPUserBundle:Gender:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Gender entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MPUserBundle:Gender')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Gender entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MPUserBundle:Gender:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Gender entity.
    *
    * @param Gender $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Gender $entity)
    {
        $form = $this->createForm(new GenderType(), $entity, array(
            'action' => $this->generateUrl('gender_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Gender entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MPUserBundle:Gender')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Gender entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('gender_edit', array('id' => $id)));
        }

        return $this->render('MPUserBundle:Gender:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Gender entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('MPUserBundle:Gender')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Gender entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('gender'));
    }

    /**
     * Creates a form to delete a Gender entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('gender_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
