<?php

namespace MP\UserBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use MP\UserBundle\Entity\Circle;
use MP\UserBundle\Form\CircleType;

/**
 * Circle controller.
 *
 */
class CircleController extends Controller
{

    /**
     * Lists all Circle entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MPUserBundle:Circle')->findAll();

        return $this->render('MPUserBundle:Circle:index.html.twig', array(
            'entities' => $entities,
        ));
    }


    public function indexUserAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $em->getRepository('MPUserBundle:User')->find($id);

//        $entities = $em->getRepository('MPUserBundle:Circle')->findAll();
        $entities = $user->getCircles();

        return $this->render('MPUserBundle:Circle:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * Creates a new Circle entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Circle();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('circle_show', array('id' => $entity->getId())));
        }

        return $this->render('MPUserBundle:Circle:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Circle entity.
     *
     * @param Circle $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Circle $entity)
    {
        $form = $this->createForm(new CircleType(), $entity, array(
            'action' => $this->generateUrl('circle_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Circle entity.
     *
     */
    public function newAction()
    {
        $entity = new Circle();
        $form   = $this->createCreateForm($entity);

        return $this->render('MPUserBundle:Circle:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Circle entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MPUserBundle:Circle')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Circle entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MPUserBundle:Circle:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Circle entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MPUserBundle:Circle')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Circle entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MPUserBundle:Circle:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Circle entity.
    *
    * @param Circle $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Circle $entity)
    {
        $form = $this->createForm(new CircleType(), $entity, array(
            'action' => $this->generateUrl('circle_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Circle entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MPUserBundle:Circle')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Circle entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('circle_edit', array('id' => $id)));
        }

        return $this->render('MPUserBundle:Circle:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Circle entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('MPUserBundle:Circle')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Circle entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('circle'));
    }

    /**
     * Creates a form to delete a Circle entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('circle_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
