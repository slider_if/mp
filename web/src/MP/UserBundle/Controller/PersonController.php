<?php

namespace MP\UserBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use MP\UserBundle\Entity\Person;
use MP\UserBundle\Form\PersonType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * Person controller.
 *
 */
class PersonController extends Controller
{

    /**
     * Lists all Person entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MPUserBundle:Person')->findAll();

        return $this->render('MPUserBundle:Person:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Person entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Person();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('person_show', array('id' => $entity->getId())));
        }

        return $this->render('MPUserBundle:Person:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Person entity.
     *
     * @param Person $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Person $entity)
    {
        $form = $this->createForm(new PersonType(), $entity, array(
            'action' => $this->generateUrl('person_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Person entity.
     *
     */
    public function newAction()
    {
        $entity = new Person();
        $form   = $this->createCreateForm($entity);

        return $this->render('MPUserBundle:Person:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Person entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MPUserBundle:Person')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Person entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $user = $this->get('security.context')->getToken()->getUser();

        return $this->render('MPUserBundle:Person:show.html.twig', array(
            'entity'      => $entity,
            'is_logged_user' => ($user->getPerson()->getId() == $id),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a User->Person entity.
     *
     */
    public function showPersonalAction()
    {
        $user = $this->get('security.context')->getToken()->getUser();
        //$entity = $user->getPerson();
        // $em = $this->getDoctrine()->getManager();

        // $entity = $em->getRepository('MPUserBundle:Person')->find($id);

        // if (!$entity) {
        //     throw $this->createNotFoundException('Unable to find Person entity.');
        // }

        $entity = new Person();

        $deleteForm = $this->createDeleteForm(1);

        return $this->render('MPUserBundle:Person:show.html.twig', array(
            'entity'      => $entity,
            'is_logged_user' => true,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Person entity.
     *
     */
    public function editAction()
    {
        $user = $this->get('security.context')->getToken()->getUser();
        $entity = $user->getPerson();
        // $em = $this->getDoctrine()->getManager();
        // $entity = $em->getRepository('MPUserBundle:Person')->findBy(array('user' => $user));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Person entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($entity->getId());

        return $this->render('MPUserBundle:Person:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Person entity.
    *
    * @param Person $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Person $entity)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new PersonType(), $entity, array(
            'action' => $this->generateUrl('person_update', array('id' => $entity->getId())),
            'method' => 'POST',
//            'em' => $em,
        ));

        $em = $this->getDoctrine()->getManager();

        $repository = $em->getRepository('MPUserBundle:Coatsu');

        $query = $repository->createQueryBuilder('c')
            ->select('c.id,c.unitname AS value')
            ->where('c.idParent IS NULL')
            ->orderBy('c.unitname', 'ASC')
            ->getQuery();

        $entities = $query->getResult();

        $addressSelect = [];
        foreach ($entities as $value) {
            $addressSelect[$value['id']] = $value['value'];
        }

        $form->add('address', 'entity', array(
            'class' => 'MPUserBundle:Coatsu',
            'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder('c')
//                        ->select('*')
                        ->where('c.idParent IS NULL')
                        ->orderBy('c.unitname', 'ASC');
                },
            'mapped' => false,
            'attr' => [
                'class' => 'select2 addressSelect',
                'data-level' => '0',
                'data-url' => $this->generateUrl('get_select'),
                'data-first' => true
            ]
        ));

//        $form->add('address', 'choice', array(
//                    'choices' => $addressSelect,
//                    'required'  => true,
//                    'attr' => [
//                        'class' => 'select2 addressSelect',
//                        'data-level' => '0',
//                        'data-url' => $this->generateUrl('get_select'),
//                        'data-first' => true
//                    ]
//                ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Person entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MPUserBundle:Person')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Person entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        if ($request->getMethod() == 'POST') {
            $requestParam = $this->getRequest()->request->all();
            $requestParam = $requestParam['mp_userbundle_person'];

            $address = $em->getRepository('MPUserBundle:Addr')->findOneById($requestParam['address']);

            $entity->setFirstName($requestParam['firstname']);
            $entity->setLastName($requestParam['lastname']);
            $entity->setSurName($requestParam['sur_name']);
            $entity->setSex($requestParam['sex']);
            $entity->setBirthday(new \DateTime($requestParam['birthday']['year'].'-'.$requestParam['birthday']['month'].'-'.$requestParam['birthday']['day']));
            $entity->setWork($requestParam['work']);
            $entity->setAddress($address);
            $em->flush();

            return $this->redirect($this->generateUrl('my_person_show'));
        }




        return $this->render('MPUserBundle:Person:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Person entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('MPUserBundle:Person')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Person entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('person'));
    }

    /**
     * Creates a form to delete a Person entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('person_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
