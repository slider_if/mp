<?php

namespace MP\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Country
 */
class Country
{
    /**
     * @var integer
     */
    private $countryId;

    /**
     * @var string
     */
    private $code;

    /**
     * @var string
     */
    private $codeAlpha2;

    /**
     * @var string
     */
    private $codeAlpha3;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $officialName;

    /**
     * @var \DateTime
     */
    private $modified;

    /**
     * @var boolean
     */
    private $canModify;

    /**
     * @var string
     */
    private $comment;


    /**
     * Get countryId
     *
     * @return integer 
     */
    public function getCountryId()
    {
        return $this->countryId;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return Country
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set codeAlpha2
     *
     * @param string $codeAlpha2
     * @return Country
     */
    public function setCodeAlpha2($codeAlpha2)
    {
        $this->codeAlpha2 = $codeAlpha2;

        return $this;
    }

    /**
     * Get codeAlpha2
     *
     * @return string 
     */
    public function getCodeAlpha2()
    {
        return $this->codeAlpha2;
    }

    /**
     * Set codeAlpha3
     *
     * @param string $codeAlpha3
     * @return Country
     */
    public function setCodeAlpha3($codeAlpha3)
    {
        $this->codeAlpha3 = $codeAlpha3;

        return $this;
    }

    /**
     * Get codeAlpha3
     *
     * @return string 
     */
    public function getCodeAlpha3()
    {
        return $this->codeAlpha3;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Country
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set officialName
     *
     * @param string $officialName
     * @return Country
     */
    public function setOfficialName($officialName)
    {
        $this->officialName = $officialName;

        return $this;
    }

    /**
     * Get officialName
     *
     * @return string 
     */
    public function getOfficialName()
    {
        return $this->officialName;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return Country
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set canModify
     *
     * @param boolean $canModify
     * @return Country
     */
    public function setCanModify($canModify)
    {
        $this->canModify = $canModify;

        return $this;
    }

    /**
     * Get canModify
     *
     * @return boolean 
     */
    public function getCanModify()
    {
        return $this->canModify;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return Country
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }
}
