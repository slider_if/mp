<?php

namespace MP\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Company
 */
class Company
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $first_name;

    /**
     * @var string
     */
    private $last_name;

    /**
     * @var string
     */
    private $sur_name;

    /**
     * @var string
     */
    private $phone;

    /**
     * @var \DateTime
     */
    private $birth_date;

    /**
     * @var integer
     */
    private $gender;

    /**
     * @var \MP\UserBundle\Entity\Person
     */
    private $user;

//    /**
//     * @var \MP\UserBundle\Entity\Appartment
//     */
//    private $address;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set first_name
     *
     * @param string $firstName
     * @return Company
     */
    public function setFirstName($firstName)
    {
        $this->first_name = $firstName;

        return $this;
    }

    /**
     * Get first_name
     *
     * @return string 
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * Set last_name
     *
     * @param string $lastName
     * @return Company
     */
    public function setLastName($lastName)
    {
        $this->last_name = $lastName;

        return $this;
    }

    /**
     * Get last_name
     *
     * @return string 
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * Set sur_name
     *
     * @param string $surName
     * @return Company
     */
    public function setSurName($surName)
    {
        $this->sur_name = $surName;

        return $this;
    }

    /**
     * Get sur_name
     *
     * @return string 
     */
    public function getSurName()
    {
        return $this->sur_name;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return Company
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set birth_date
     *
     * @param \DateTime $birthDate
     * @return Company
     */
    public function setBirthDate($birthDate)
    {
        $this->birth_date = $birthDate;

        return $this;
    }

    /**
     * Get birth_date
     *
     * @return \DateTime 
     */
    public function getBirthDate()
    {
        return $this->birth_date;
    }

    /**
     * Set gender
     *
     * @param integer $gender
     * @return Company
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return integer 
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set user
     *
     * @param \MP\UserBundle\Entity\Person $user
     * @return Company
     */
    public function setUser(\MP\UserBundle\Entity\Person $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \MP\UserBundle\Entity\Person 
     */
    public function getUser()
    {
        return $this->user;
    }

//    /**
//     * Set address
//     *
//     * @param \MP\UserBundle\Entity\Appartment $address
//     * @return Company
//     */
//    public function setAddress(\MP\UserBundle\Entity\Appartment $address = null)
//    {
//        $this->address = $address;
//
//        return $this;
//    }

//    /**
//     * Get address
//     *
//     * @return \MP\UserBundle\Entity\Appartment
//     */
//    public function getAddress()
//    {
//        return $this->address;
//    }
//    /**
//     * @var \MP\UserBundle\Entity\Person
//     */
    private $person;


    /**
     * Set person
     *
     * @param \MP\UserBundle\Entity\Person $person
     * @return Company
     */
    public function setPerson(\MP\UserBundle\Entity\Person $person = null)
    {
        $this->person = $person;

        return $this;
    }

    /**
     * Get person
     *
     * @return \MP\UserBundle\Entity\Person 
     */
    public function getPerson()
    {
        return $this->person;
    }
    /**
     * @var string
     */
    private $company_name;

    /**
     * @var string
     */
    private $contact_name;

    /**
     * @var string
     */
    private $manyToOne;

    /**
     * @var string
     */
    private $about;

    /**
     * @var string
     */
    private $work_type;

    /**
     * @var string
     */
    private $foundation_year;

    /**
     * @var string
     */
    private $opf;

    /**
     * @var string
     */
    private $uf;

    /**
     * @var integer
     */
    private $stuff;

    /**
     * @var string
     */
    private $brends;

    /**
     * @var string
     */
    private $clients;

    /**
     * @var string
     */
    private $sales_a_year;

    /**
     * @var string
     */
    private $certificates;


    /**
     * Set company_name
     *
     * @param string $companyName
     * @return Company
     */
    public function setCompanyName($companyName)
    {
        $this->company_name = $companyName;

        return $this;
    }

    /**
     * Get company_name
     *
     * @return string 
     */
    public function getCompanyName()
    {
        return $this->company_name;
    }

    /**
     * Set contact_name
     *
     * @param string $contactName
     * @return Company
     */
    public function setContactName($contactName)
    {
        $this->contact_name = $contactName;

        return $this;
    }

    /**
     * Get contact_name
     *
     * @return string 
     */
    public function getContactName()
    {
        return $this->contact_name;
    }

    /**
     * Set manyToOne
     *
     * @param string $manyToOne
     * @return Company
     */
    public function setManyToOne($manyToOne)
    {
        $this->manyToOne = $manyToOne;

        return $this;
    }

    /**
     * Get manyToOne
     *
     * @return string 
     */
    public function getManyToOne()
    {
        return $this->manyToOne;
    }

    /**
     * Set about
     *
     * @param string $about
     * @return Company
     */
    public function setAbout($about)
    {
        $this->about = $about;

        return $this;
    }

    /**
     * Get about
     *
     * @return string 
     */
    public function getAbout()
    {
        return $this->about;
    }

    /**
     * Set work_type
     *
     * @param string $workType
     * @return Company
     */
    public function setWorkType($workType)
    {
        $this->work_type = $workType;

        return $this;
    }

    /**
     * Get work_type
     *
     * @return string 
     */
    public function getWorkType()
    {
        return $this->work_type;
    }

    /**
     * Set foundation_year
     *
     * @param string $foundationYear
     * @return Company
     */
    public function setFoundationYear($foundationYear)
    {
        $this->foundation_year = $foundationYear;

        return $this;
    }

    /**
     * Get foundation_year
     *
     * @return string 
     */
    public function getFoundationYear()
    {
        return $this->foundation_year;
    }

    /**
     * Set opf
     *
     * @param string $opf
     * @return Company
     */
    public function setOpf($opf)
    {
        $this->opf = $opf;

        return $this;
    }

    /**
     * Get opf
     *
     * @return string 
     */
    public function getOpf()
    {
        return $this->opf;
    }

    /**
     * Set uf
     *
     * @param string $uf
     * @return Company
     */
    public function setUf($uf)
    {
        $this->uf = $uf;

        return $this;
    }

    /**
     * Get uf
     *
     * @return string 
     */
    public function getUf()
    {
        return $this->uf;
    }

    /**
     * Set stuff
     *
     * @param integer $stuff
     * @return Company
     */
    public function setStuff($stuff)
    {
        $this->stuff = $stuff;

        return $this;
    }

    /**
     * Get stuff
     *
     * @return integer 
     */
    public function getStuff()
    {
        return $this->stuff;
    }

    /**
     * Set brends
     *
     * @param string $brends
     * @return Company
     */
    public function setBrends($brends)
    {
        $this->brends = $brends;

        return $this;
    }

    /**
     * Get brends
     *
     * @return string 
     */
    public function getBrends()
    {
        return $this->brends;
    }

    /**
     * Set clients
     *
     * @param string $clients
     * @return Company
     */
    public function setClients($clients)
    {
        $this->clients = $clients;

        return $this;
    }

    /**
     * Get clients
     *
     * @return string 
     */
    public function getClients()
    {
        return $this->clients;
    }

    /**
     * Set sales_a_year
     *
     * @param string $salesAYear
     * @return Company
     */
    public function setSalesAYear($salesAYear)
    {
        $this->sales_a_year = $salesAYear;

        return $this;
    }

    /**
     * Get sales_a_year
     *
     * @return string 
     */
    public function getSalesAYear()
    {
        return $this->sales_a_year;
    }

    /**
     * Set certificates
     *
     * @param string $certificates
     * @return Company
     */
    public function setCertificates($certificates)
    {
        $this->certificates = $certificates;

        return $this;
    }

    /**
     * Get certificates
     *
     * @return string 
     */
    public function getCertificates()
    {
        return $this->certificates;
    }
}
