<?php

namespace MP\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AdministrativeUnit
 */
class AdministrativeUnit
{
    /**
     * @var integer
     */
    private $administrativeUnitId;

    /**
     * @var string
     */
    private $code;

    /**
     * @var string
     */
    private $unitType;

    /**
     * @var string
     */
    private $unitName;

    /**
     * @var boolean
     */
    private $canModify;

    /**
     * @var \DateTime
     */
    private $modified;

    /**
     * @var string
     */
    private $comment;

    /**
     * @var \MP\UserBundle\Entity\Country
     */
    private $country;

    /**
     * @var \MP\UserBundle\Entity\AdministrativeUnit
     */
    private $parent;


    /**
     * Get administrativeUnitId
     *
     * @return integer 
     */
    public function getAdministrativeUnitId()
    {
        return $this->administrativeUnitId;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return AdministrativeUnit
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set unitType
     *
     * @param string $unitType
     * @return AdministrativeUnit
     */
    public function setUnitType($unitType)
    {
        $this->unitType = $unitType;

        return $this;
    }

    /**
     * Get unitType
     *
     * @return string 
     */
    public function getUnitType()
    {
        return $this->unitType;
    }

    /**
     * Set unitName
     *
     * @param string $unitName
     * @return AdministrativeUnit
     */
    public function setUnitName($unitName)
    {
        $this->unitName = $unitName;

        return $this;
    }

    /**
     * Get unitName
     *
     * @return string 
     */
    public function getUnitName()
    {
        return $this->unitName;
    }

    /**
     * Set canModify
     *
     * @param boolean $canModify
     * @return AdministrativeUnit
     */
    public function setCanModify($canModify)
    {
        $this->canModify = $canModify;

        return $this;
    }

    /**
     * Get canModify
     *
     * @return boolean 
     */
    public function getCanModify()
    {
        return $this->canModify;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return AdministrativeUnit
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return AdministrativeUnit
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set country
     *
     * @param \MP\UserBundle\Entity\Country $country
     * @return AdministrativeUnit
     */
    public function setCountry(\MP\UserBundle\Entity\Country $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return \MP\UserBundle\Entity\Country 
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set parent
     *
     * @param \MP\UserBundle\Entity\AdministrativeUnit $parent
     * @return AdministrativeUnit
     */
    public function setParent(\MP\UserBundle\Entity\AdministrativeUnit $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \MP\UserBundle\Entity\AdministrativeUnit 
     */
    public function getParent()
    {
        return $this->parent;
    }
}
