<?php

namespace MP\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Building
 */
class Building
{
    /**
     * @var integer
     */
    private $buildingId;

    /**
     * @var string
     */
    private $buildingNo;

    /**
     * @var string
     */
    private $postalCode;

    /**
     * @var string
     */
    private $latitude;

    /**
     * @var string
     */
    private $longitude;

    /**
     * @var \MP\UserBundle\Entity\Country
     */
    private $country;

    /**
     * @var \MP\UserBundle\Entity\Street
     */
    private $street;


    /**
     * Get buildingId
     *
     * @return integer 
     */
    public function getBuildingId()
    {
        return $this->buildingId;
    }

    /**
     * Set buildingNo
     *
     * @param string $buildingNo
     * @return Building
     */
    public function setBuildingNo($buildingNo)
    {
        $this->buildingNo = $buildingNo;

        return $this;
    }

    /**
     * Get buildingNo
     *
     * @return string 
     */
    public function getBuildingNo()
    {
        return $this->buildingNo;
    }

    /**
     * Set postalCode
     *
     * @param string $postalCode
     * @return Building
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    /**
     * Get postalCode
     *
     * @return string 
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * Set latitude
     *
     * @param string $latitude
     * @return Building
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return string 
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @param string $longitude
     * @return Building
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return string 
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set country
     *
     * @param \MP\UserBundle\Entity\Country $country
     * @return Building
     */
    public function setCountry(\MP\UserBundle\Entity\Country $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return \MP\UserBundle\Entity\Country 
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set street
     *
     * @param \MP\UserBundle\Entity\Street $street
     * @return Building
     */
    public function setStreet(\MP\UserBundle\Entity\Street $street = null)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get street
     *
     * @return \MP\UserBundle\Entity\Street 
     */
    public function getStreet()
    {
        return $this->street;
    }
}
