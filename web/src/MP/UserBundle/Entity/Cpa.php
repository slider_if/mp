<?php

namespace MP\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cpa
 */
class Cpa
{
    /**
     * @var integer
     */
    private $cpaId;

    /**
     * @var string
     */
    private $code;

    /**
     * @var string
     */
    private $codeParent;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $short;

    /**
     * @var string
     */
    private $documentNo;

    /**
     * @var \DateTime
     */
    private $modified;

    /**
     * @var boolean
     */
    private $canModify;

    /**
     * @var string
     */
    private $comment;

    /**
     * @var \MP\UserBundle\Entity\Cpa
     */
    private $parent;


    /**
     * Get cpaId
     *
     * @return integer 
     */
    public function getCpaId()
    {
        return $this->cpaId;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return Cpa
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set codeParent
     *
     * @param string $codeParent
     * @return Cpa
     */
    public function setCodeParent($codeParent)
    {
        $this->codeParent = $codeParent;

        return $this;
    }

    /**
     * Get codeParent
     *
     * @return string 
     */
    public function getCodeParent()
    {
        return $this->codeParent;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Cpa
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set short
     *
     * @param string $short
     * @return Cpa
     */
    public function setShort($short)
    {
        $this->short = $short;

        return $this;
    }

    /**
     * Get short
     *
     * @return string 
     */
    public function getShort()
    {
        return $this->short;
    }

    /**
     * Set documentNo
     *
     * @param string $documentNo
     * @return Cpa
     */
    public function setDocumentNo($documentNo)
    {
        $this->documentNo = $documentNo;

        return $this;
    }

    /**
     * Get documentNo
     *
     * @return string 
     */
    public function getDocumentNo()
    {
        return $this->documentNo;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return Cpa
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set canModify
     *
     * @param boolean $canModify
     * @return Cpa
     */
    public function setCanModify($canModify)
    {
        $this->canModify = $canModify;

        return $this;
    }

    /**
     * Get canModify
     *
     * @return boolean 
     */
    public function getCanModify()
    {
        return $this->canModify;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return Cpa
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set parent
     *
     * @param \MP\UserBundle\Entity\Cpa $parent
     * @return Cpa
     */
    public function setParent(\MP\UserBundle\Entity\Cpa $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \MP\UserBundle\Entity\Cpa 
     */
    public function getParent()
    {
        return $this->parent;
    }
}
