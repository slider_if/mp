<?php

namespace MP\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Postalvz
 */
class Postalvz
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $zip;

    /**
     * @var string
     */
    private $postoffice;

    /**
     * @var string
     */
    private $region;

    /**
     * @var string
     */
    private $district;

    /**
     * @var string
     */
    private $settlement;

    /**
     * @var string
     */
    private $address;

    /**
     * @var string
     */
    private $phone;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set zip
     *
     * @param string $zip
     * @return Postalvz
     */
    public function setZip($zip)
    {
        $this->zip = $zip;

        return $this;
    }

    /**
     * Get zip
     *
     * @return string 
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * Set postoffice
     *
     * @param string $postoffice
     * @return Postalvz
     */
    public function setPostoffice($postoffice)
    {
        $this->postoffice = $postoffice;

        return $this;
    }

    /**
     * Get postoffice
     *
     * @return string 
     */
    public function getPostoffice()
    {
        return $this->postoffice;
    }

    /**
     * Set region
     *
     * @param string $region
     * @return Postalvz
     */
    public function setRegion($region)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return string 
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set district
     *
     * @param string $district
     * @return Postalvz
     */
    public function setDistrict($district)
    {
        $this->district = $district;

        return $this;
    }

    /**
     * Get district
     *
     * @return string 
     */
    public function getDistrict()
    {
        return $this->district;
    }

    /**
     * Set settlement
     *
     * @param string $settlement
     * @return Postalvz
     */
    public function setSettlement($settlement)
    {
        $this->settlement = $settlement;

        return $this;
    }

    /**
     * Get settlement
     *
     * @return string 
     */
    public function getSettlement()
    {
        return $this->settlement;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Postalvz
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return Postalvz
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }
}
