<?php

namespace MP\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Addr
 */
class Addr
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $koatuu;

    /**
     * @var string
     */
    private $postcode;

    /**
     * @var string
     */
    private $phone;

    /**
     * @var integer
     */
    private $idCoatsu;

    /**
     * @var string
     */
    private $data;

    /**
     * @var string
     */
    private $house;

    /**
     * @var string
     */
    private $streettype;

    /**
     * @var string
     */
    private $streetname;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Addr
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set koatuu
     *
     * @param string $koatuu
     * @return Addr
     */
    public function setKoatuu($koatuu)
    {
        $this->koatuu = $koatuu;

        return $this;
    }

    /**
     * Get koatuu
     *
     * @return string 
     */
    public function getKoatuu()
    {
        return $this->koatuu;
    }

    /**
     * Set postcode
     *
     * @param string $postcode
     * @return Addr
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;

        return $this;
    }

    /**
     * Get postcode
     *
     * @return string 
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return Addr
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set idCoatsu
     *
     * @param integer $idCoatsu
     * @return Addr
     */
    public function setIdCoatsu($idCoatsu)
    {
        $this->idCoatsu = $idCoatsu;

        return $this;
    }

    /**
     * Get idCoatsu
     *
     * @return integer 
     */
    public function getIdCoatsu()
    {
        return $this->idCoatsu;
    }

    /**
     * Set data
     *
     * @param string $data
     * @return Addr
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data
     *
     * @return string 
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set house
     *
     * @param string $house
     * @return Addr
     */
    public function setHouse($house)
    {
        $this->house = $house;

        return $this;
    }

    /**
     * Get house
     *
     * @return string 
     */
    public function getHouse()
    {
        return $this->house;
    }

    /**
     * Set streettype
     *
     * @param string $streettype
     * @return Addr
     */
    public function setStreettype($streettype)
    {
        $this->streettype = $streettype;

        return $this;
    }

    /**
     * Get streettype
     *
     * @return string 
     */
    public function getStreettype()
    {
        return $this->streettype;
    }

    /**
     * Set streetname
     *
     * @param string $streetname
     * @return Addr
     */
    public function setStreetname($streetname)
    {
        $this->streetname = $streetname;

        return $this;
    }

    /**
     * Get streetname
     *
     * @return string 
     */
    public function getStreetname()
    {
        return $this->streetname;
    }

    public  function __toString(){
        return $this->getName();
    }
}
