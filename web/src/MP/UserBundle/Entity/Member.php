<?php

namespace MP\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Member
 */
class Member
{
    /**
     * @var integer
     */
    private $memberId;

    /**
     * @var boolean
     */
    private $isLocked;

    /**
     * @var \MP\UserBundle\Entity\Community
     */
    private $community;

    /**
     * @var \MP\UserBundle\Entity\Users
     */
    private $user;


    /**
     * Get memberId
     *
     * @return integer 
     */
    public function getMemberId()
    {
        return $this->memberId;
    }

    /**
     * Set isLocked
     *
     * @param boolean $isLocked
     * @return Member
     */
    public function setIsLocked($isLocked)
    {
        $this->isLocked = $isLocked;

        return $this;
    }

    /**
     * Get isLocked
     *
     * @return boolean 
     */
    public function getIsLocked()
    {
        return $this->isLocked;
    }

    /**
     * Set community
     *
     * @param \MP\UserBundle\Entity\Community $community
     * @return Member
     */
    public function setCommunity(\MP\UserBundle\Entity\Community $community = null)
    {
        $this->community = $community;

        return $this;
    }

    /**
     * Get community
     *
     * @return \MP\UserBundle\Entity\Community 
     */
    public function getCommunity()
    {
        return $this->community;
    }

    /**
     * Set user
     *
     * @param \MP\UserBundle\Entity\Users $user
     * @return Member
     */
    public function setUser(\MP\UserBundle\Entity\Users $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \MP\UserBundle\Entity\Users 
     */
    public function getUser()
    {
        return $this->user;
    }
}
