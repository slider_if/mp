<?php

namespace MP\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Community
 */
class Community
{
    /**
     * @var integer
     */
    private $communityId;

    /**
     * @var integer
     */
    private $communityType;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $comment;

    /**
     * @var \MP\UserBundle\Entity\Community
     */
    private $parent;


    /**
     * Get communityId
     *
     * @return integer 
     */
    public function getCommunityId()
    {
        return $this->communityId;
    }

    /**
     * Set communityType
     *
     * @param integer $communityType
     * @return Community
     */
    public function setCommunityType($communityType)
    {
        $this->communityType = $communityType;

        return $this;
    }

    /**
     * Get communityType
     *
     * @return integer 
     */
    public function getCommunityType()
    {
        return $this->communityType;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Community
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return Community
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set parent
     *
     * @param \MP\UserBundle\Entity\Community $parent
     * @return Community
     */
    public function setParent(\MP\UserBundle\Entity\Community $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \MP\UserBundle\Entity\Community 
     */
    public function getParent()
    {
        return $this->parent;
    }
}
