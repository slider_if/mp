<?php

namespace MP\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Address
 */
class Address
{
    /**
     * @var integer
     */
    private $addressId;

    /**
     * @var integer
     */
    private $addressType;

    /**
     * @var string
     */
    private $address;

    /**
     * @var \MP\UserBundle\Entity\AdministrativeUnit
     */
    private $administrativeUnit;

    /**
     * @var \MP\UserBundle\Entity\Building
     */
    private $building;

    /**
     * @var \MP\UserBundle\Entity\Country
     */
    private $country;

    /**
     * @var \MP\UserBundle\Entity\Person
     */
    private $person;

    /**
     * @var \MP\UserBundle\Entity\Street
     */
    private $street;


    /**
     * Get addressId
     *
     * @return integer 
     */
    public function getAddressId()
    {
        return $this->addressId;
    }

    /**
     * Set addressType
     *
     * @param integer $addressType
     * @return Address
     */
    public function setAddressType($addressType)
    {
        $this->addressType = $addressType;

        return $this;
    }

    /**
     * Get addressType
     *
     * @return integer 
     */
    public function getAddressType()
    {
        return $this->addressType;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Address
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set administrativeUnit
     *
     * @param \MP\UserBundle\Entity\AdministrativeUnit $administrativeUnit
     * @return Address
     */
    public function setAdministrativeUnit(\MP\UserBundle\Entity\AdministrativeUnit $administrativeUnit = null)
    {
        $this->administrativeUnit = $administrativeUnit;

        return $this;
    }

    /**
     * Get administrativeUnit
     *
     * @return \MP\UserBundle\Entity\AdministrativeUnit 
     */
    public function getAdministrativeUnit()
    {
        return $this->administrativeUnit;
    }

    /**
     * Set building
     *
     * @param \MP\UserBundle\Entity\Building $building
     * @return Address
     */
    public function setBuilding(\MP\UserBundle\Entity\Building $building = null)
    {
        $this->building = $building;

        return $this;
    }

    /**
     * Get building
     *
     * @return \MP\UserBundle\Entity\Building 
     */
    public function getBuilding()
    {
        return $this->building;
    }

    /**
     * Set country
     *
     * @param \MP\UserBundle\Entity\Country $country
     * @return Address
     */
    public function setCountry(\MP\UserBundle\Entity\Country $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return \MP\UserBundle\Entity\Country 
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set person
     *
     * @param \MP\UserBundle\Entity\Person $person
     * @return Address
     */
    public function setPerson(\MP\UserBundle\Entity\Person $person = null)
    {
        $this->person = $person;

        return $this;
    }

    /**
     * Get person
     *
     * @return \MP\UserBundle\Entity\Person 
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * Set street
     *
     * @param \MP\UserBundle\Entity\Street $street
     * @return Address
     */
    public function setStreet(\MP\UserBundle\Entity\Street $street = null)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get street
     *
     * @return \MP\UserBundle\Entity\Street 
     */
    public function getStreet()
    {
        return $this->street;
    }
}
