<?php

namespace MP\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserProfile
 */
class UserProfile
{
    /**
     * @var integer
     */
    private $profileId;

    /**
     * @var integer
     */
    private $attributeId;

    /**
     * @var integer
     */
    private $selector;

    /**
     * @var \DateTime
     */
    private $modified;

    /**
     * @var \MP\UserBundle\Entity\ProfileType
     */
    private $profileType;

    /**
     * @var \MP\UserBundle\Entity\Users
     */
    private $user;


    /**
     * Get profileId
     *
     * @return integer 
     */
    public function getProfileId()
    {
        return $this->profileId;
    }

    /**
     * Set attributeId
     *
     * @param integer $attributeId
     * @return UserProfile
     */
    public function setAttributeId($attributeId)
    {
        $this->attributeId = $attributeId;

        return $this;
    }

    /**
     * Get attributeId
     *
     * @return integer 
     */
    public function getAttributeId()
    {
        return $this->attributeId;
    }

    /**
     * Set selector
     *
     * @param integer $selector
     * @return UserProfile
     */
    public function setSelector($selector)
    {
        $this->selector = $selector;

        return $this;
    }

    /**
     * Get selector
     *
     * @return integer 
     */
    public function getSelector()
    {
        return $this->selector;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return UserProfile
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set profileType
     *
     * @param \MP\UserBundle\Entity\ProfileType $profileType
     * @return UserProfile
     */
    public function setProfileType(\MP\UserBundle\Entity\ProfileType $profileType = null)
    {
        $this->profileType = $profileType;

        return $this;
    }

    /**
     * Get profileType
     *
     * @return \MP\UserBundle\Entity\ProfileType 
     */
    public function getProfileType()
    {
        return $this->profileType;
    }

    /**
     * Set user
     *
     * @param \MP\UserBundle\Entity\Users $user
     * @return UserProfile
     */
    public function setUser(\MP\UserBundle\Entity\Users $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \MP\UserBundle\Entity\Users 
     */
    public function getUser()
    {
        return $this->user;
    }
}
