<?php

namespace MP\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cea
 */
class Cea
{
    /**
     * @var integer
     */
    private $ceaId;

    /**
     * @var string
     */
    private $section;

    /**
     * @var string
     */
    private $code;

    /**
     * @var string
     */
    private $title;

    /**
     * @var \DateTime
     */
    private $modified;

    /**
     * @var boolean
     */
    private $canModify;

    /**
     * @var string
     */
    private $comment;

    /**
     * @var \MP\UserBundle\Entity\Cea
     */
    private $parent;


    /**
     * Get ceaId
     *
     * @return integer 
     */
    public function getCeaId()
    {
        return $this->ceaId;
    }

    /**
     * Set section
     *
     * @param string $section
     * @return Cea
     */
    public function setSection($section)
    {
        $this->section = $section;

        return $this;
    }

    /**
     * Get section
     *
     * @return string 
     */
    public function getSection()
    {
        return $this->section;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return Cea
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Cea
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return Cea
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set canModify
     *
     * @param boolean $canModify
     * @return Cea
     */
    public function setCanModify($canModify)
    {
        $this->canModify = $canModify;

        return $this;
    }

    /**
     * Get canModify
     *
     * @return boolean 
     */
    public function getCanModify()
    {
        return $this->canModify;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return Cea
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set parent
     *
     * @param \MP\UserBundle\Entity\Cea $parent
     * @return Cea
     */
    public function setParent(\MP\UserBundle\Entity\Cea $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \MP\UserBundle\Entity\Cea 
     */
    public function getParent()
    {
        return $this->parent;
    }
}
