<?php

namespace MP\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Postalindex
 */
class Postalindex
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $region;

    /**
     * @var string
     */
    private $district;

    /**
     * @var string
     */
    private $settlement;

    /**
     * @var string
     */
    private $code;

    /**
     * @var string
     */
    private $regionEn;

    /**
     * @var string
     */
    private $districtEn;

    /**
     * @var string
     */
    private $settlementEn;

    /**
     * @var string
     */
    private $regionRu;

    /**
     * @var string
     */
    private $districtRu;

    /**
     * @var string
     */
    private $settlementRu;

    /**
     * @var string
     */
    private $postoffice;

    /**
     * @var string
     */
    private $postofficeEn;

    /**
     * @var string
     */
    private $postofficeRu;

    /**
     * @var string
     */
    private $postofficecode;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set region
     *
     * @param string $region
     * @return Postalindex
     */
    public function setRegion($region)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return string 
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set district
     *
     * @param string $district
     * @return Postalindex
     */
    public function setDistrict($district)
    {
        $this->district = $district;

        return $this;
    }

    /**
     * Get district
     *
     * @return string 
     */
    public function getDistrict()
    {
        return $this->district;
    }

    /**
     * Set settlement
     *
     * @param string $settlement
     * @return Postalindex
     */
    public function setSettlement($settlement)
    {
        $this->settlement = $settlement;

        return $this;
    }

    /**
     * Get settlement
     *
     * @return string 
     */
    public function getSettlement()
    {
        return $this->settlement;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return Postalindex
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set regionEn
     *
     * @param string $regionEn
     * @return Postalindex
     */
    public function setRegionEn($regionEn)
    {
        $this->regionEn = $regionEn;

        return $this;
    }

    /**
     * Get regionEn
     *
     * @return string 
     */
    public function getRegionEn()
    {
        return $this->regionEn;
    }

    /**
     * Set districtEn
     *
     * @param string $districtEn
     * @return Postalindex
     */
    public function setDistrictEn($districtEn)
    {
        $this->districtEn = $districtEn;

        return $this;
    }

    /**
     * Get districtEn
     *
     * @return string 
     */
    public function getDistrictEn()
    {
        return $this->districtEn;
    }

    /**
     * Set settlementEn
     *
     * @param string $settlementEn
     * @return Postalindex
     */
    public function setSettlementEn($settlementEn)
    {
        $this->settlementEn = $settlementEn;

        return $this;
    }

    /**
     * Get settlementEn
     *
     * @return string 
     */
    public function getSettlementEn()
    {
        return $this->settlementEn;
    }

    /**
     * Set regionRu
     *
     * @param string $regionRu
     * @return Postalindex
     */
    public function setRegionRu($regionRu)
    {
        $this->regionRu = $regionRu;

        return $this;
    }

    /**
     * Get regionRu
     *
     * @return string 
     */
    public function getRegionRu()
    {
        return $this->regionRu;
    }

    /**
     * Set districtRu
     *
     * @param string $districtRu
     * @return Postalindex
     */
    public function setDistrictRu($districtRu)
    {
        $this->districtRu = $districtRu;

        return $this;
    }

    /**
     * Get districtRu
     *
     * @return string 
     */
    public function getDistrictRu()
    {
        return $this->districtRu;
    }

    /**
     * Set settlementRu
     *
     * @param string $settlementRu
     * @return Postalindex
     */
    public function setSettlementRu($settlementRu)
    {
        $this->settlementRu = $settlementRu;

        return $this;
    }

    /**
     * Get settlementRu
     *
     * @return string 
     */
    public function getSettlementRu()
    {
        return $this->settlementRu;
    }

    /**
     * Set postoffice
     *
     * @param string $postoffice
     * @return Postalindex
     */
    public function setPostoffice($postoffice)
    {
        $this->postoffice = $postoffice;

        return $this;
    }

    /**
     * Get postoffice
     *
     * @return string 
     */
    public function getPostoffice()
    {
        return $this->postoffice;
    }

    /**
     * Set postofficeEn
     *
     * @param string $postofficeEn
     * @return Postalindex
     */
    public function setPostofficeEn($postofficeEn)
    {
        $this->postofficeEn = $postofficeEn;

        return $this;
    }

    /**
     * Get postofficeEn
     *
     * @return string 
     */
    public function getPostofficeEn()
    {
        return $this->postofficeEn;
    }

    /**
     * Set postofficeRu
     *
     * @param string $postofficeRu
     * @return Postalindex
     */
    public function setPostofficeRu($postofficeRu)
    {
        $this->postofficeRu = $postofficeRu;

        return $this;
    }

    /**
     * Get postofficeRu
     *
     * @return string 
     */
    public function getPostofficeRu()
    {
        return $this->postofficeRu;
    }

    /**
     * Set postofficecode
     *
     * @param string $postofficecode
     * @return Postalindex
     */
    public function setPostofficecode($postofficecode)
    {
        $this->postofficecode = $postofficecode;

        return $this;
    }

    /**
     * Get postofficecode
     *
     * @return string 
     */
    public function getPostofficecode()
    {
        return $this->postofficecode;
    }
}
