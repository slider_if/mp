<?php

namespace MP\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Scps
 */
class Scps
{
    /**
     * @var integer
     */
    private $scpsId;

    /**
     * @var string
     */
    private $code;

    /**
     * @var string
     */
    private $grade;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $ucpfeaCodes;

    /**
     * @var \DateTime
     */
    private $modified;

    /**
     * @var boolean
     */
    private $canModify;

    /**
     * @var string
     */
    private $comment;

    /**
     * @var \MP\UserBundle\Entity\Cea
     */
    private $cea;

    /**
     * @var \MP\UserBundle\Entity\Scps
     */
    private $parent;


    /**
     * Get scpsId
     *
     * @return integer 
     */
    public function getScpsId()
    {
        return $this->scpsId;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return Scps
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set grade
     *
     * @param string $grade
     * @return Scps
     */
    public function setGrade($grade)
    {
        $this->grade = $grade;

        return $this;
    }

    /**
     * Get grade
     *
     * @return string 
     */
    public function getGrade()
    {
        return $this->grade;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Scps
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set ucpfeaCodes
     *
     * @param string $ucpfeaCodes
     * @return Scps
     */
    public function setUcpfeaCodes($ucpfeaCodes)
    {
        $this->ucpfeaCodes = $ucpfeaCodes;

        return $this;
    }

    /**
     * Get ucpfeaCodes
     *
     * @return string 
     */
    public function getUcpfeaCodes()
    {
        return $this->ucpfeaCodes;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return Scps
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set canModify
     *
     * @param boolean $canModify
     * @return Scps
     */
    public function setCanModify($canModify)
    {
        $this->canModify = $canModify;

        return $this;
    }

    /**
     * Get canModify
     *
     * @return boolean 
     */
    public function getCanModify()
    {
        return $this->canModify;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return Scps
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set cea
     *
     * @param \MP\UserBundle\Entity\Cea $cea
     * @return Scps
     */
    public function setCea(\MP\UserBundle\Entity\Cea $cea = null)
    {
        $this->cea = $cea;

        return $this;
    }

    /**
     * Get cea
     *
     * @return \MP\UserBundle\Entity\Cea 
     */
    public function getCea()
    {
        return $this->cea;
    }

    /**
     * Set parent
     *
     * @param \MP\UserBundle\Entity\Scps $parent
     * @return Scps
     */
    public function setParent(\MP\UserBundle\Entity\Scps $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \MP\UserBundle\Entity\Scps 
     */
    public function getParent()
    {
        return $this->parent;
    }
}
