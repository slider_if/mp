<?php

namespace MP\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Circle
 */
class Circle
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $description;

    /**
     * @var integer
     */
    private $type;

    /**
     * @var integer
     */
    private $parent_id;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $users;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->users = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Circle
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Circle
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return Circle
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set parent_id
     *
     * @param integer $parentId
     * @return Circle
     */
    public function setParentId($parentId)
    {
        $this->parent_id = $parentId;

        return $this;
    }

    /**
     * Get parent_id
     *
     * @return integer 
     */
    public function getParentId()
    {
        return $this->parent_id;
    }

    /**
     * Add users
     *
     * @param \MP\UserBundle\Entity\User $users
     * @return Circle
     */
    public function addUser(\MP\UserBundle\Entity\User $users)
    {
        $this->users[] = $users;

        return $this;
    }

    /**
     * Remove users
     *
     * @param \MP\UserBundle\Entity\User $users
     */
    public function removeUser(\MP\UserBundle\Entity\User $users)
    {
        $this->users->removeElement($users);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUsers()
    {
        return $this->users;
    }
    /**
     * @var \MP\UserBundle\Entity\Coatsu
     */
    private $coatsu;


    /**
     * Set coatsu
     *
     * @param \MP\UserBundle\Entity\Coatsu $coatsu
     * @return Circle
     */
    public function setCoatsu(\MP\UserBundle\Entity\Coatsu $coatsu = null)
    {
        $this->coatsu = $coatsu;

        return $this;
    }

    /**
     * Get coatsu
     *
     * @return \MP\UserBundle\Entity\Coatsu 
     */
    public function getCoatsu()
    {
        return $this->coatsu;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $questions;


    /**
     * Add questions
     *
     * @param \MP\SocialBundle\Entity\Question $questions
     * @return Circle
     */
    public function addQuestion(\MP\SocialBundle\Entity\Question $questions)
    {
        $this->questions[] = $questions;

        return $this;
    }

    /**
     * Remove questions
     *
     * @param \MP\SocialBundle\Entity\Question $questions
     */
    public function removeQuestion(\MP\SocialBundle\Entity\Question $questions)
    {
        $this->questions->removeElement($questions);
    }

    /**
     * Get questions
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getQuestions()
    {
        return $this->questions;
    }
}
