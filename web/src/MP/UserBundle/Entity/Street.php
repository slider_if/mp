<?php

namespace MP\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Street
 */
class Street
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Street
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }


    public function __toString()
    {
        return $this->name;
    }
    /**
     * @var integer
     */
    private $idKind;

    /**
     * @var string
     */
    private $kind;

    /**
     * @var string
     */
    private $title;

    /**
     * @var \MP\UserBundle\Entity\Common.country
     */
    private $idCountry;

    /**
     * @var \MP\UserBundle\Entity\Common.coatsu
     */
    private $idCoatsu;


    /**
     * Set idKind
     *
     * @param integer $idKind
     * @return Street
     */
    public function setIdKind($idKind)
    {
        $this->idKind = $idKind;

        return $this;
    }

    /**
     * Get idKind
     *
     * @return integer 
     */
    public function getIdKind()
    {
        return $this->idKind;
    }

    /**
     * Set kind
     *
     * @param string $kind
     * @return Street
     */
    public function setKind($kind)
    {
        $this->kind = $kind;

        return $this;
    }

    /**
     * Get kind
     *
     * @return string 
     */
    public function getKind()
    {
        return $this->kind;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Street
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set idCountry
     *
     * @param \MP\UserBundle\Entity\Common.country $idCountry
     * @return Street
     */
    public function setIdCountry(\MP\UserBundle\Entity\Country $idCountry = null)
    {
        $this->idCountry = $idCountry;

        return $this;
    }

    /**
     * Get idCountry
     *
     * @return \MP\UserBundle\Entity\Common.country 
     */
    public function getIdCountry()
    {
        return $this->idCountry;
    }

    /**
     * Set idCoatsu
     *
     * @param \MP\UserBundle\Entity\Common.coatsu $idCoatsu
     * @return Street
     */
    public function setIdCoatsu(\MP\UserBundle\Entity\Coatsu $idCoatsu = null)
    {
        $this->idCoatsu = $idCoatsu;

        return $this;
    }

    /**
     * Get idCoatsu
     *
     * @return \MP\UserBundle\Entity\Common.coatsu 
     */
    public function getIdCoatsu()
    {
        return $this->idCoatsu;
    }
    /**
     * @var integer
     */
    private $streetId;

    /**
     * @var integer
     */
    private $kindType;

    /**
     * @var string
     */
    private $kindName;

    /**
     * @var \MP\UserBundle\Entity\AdministrativeUnit
     */
    private $administrativeUnit;

    /**
     * @var \MP\UserBundle\Entity\Country
     */
    private $country;


    /**
     * Get streetId
     *
     * @return integer 
     */
    public function getStreetId()
    {
        return $this->streetId;
    }

    /**
     * Set kindType
     *
     * @param integer $kindType
     * @return Street
     */
    public function setKindType($kindType)
    {
        $this->kindType = $kindType;

        return $this;
    }

    /**
     * Get kindType
     *
     * @return integer 
     */
    public function getKindType()
    {
        return $this->kindType;
    }

    /**
     * Set kindName
     *
     * @param string $kindName
     * @return Street
     */
    public function setKindName($kindName)
    {
        $this->kindName = $kindName;

        return $this;
    }

    /**
     * Get kindName
     *
     * @return string 
     */
    public function getKindName()
    {
        return $this->kindName;
    }

    /**
     * Set administrativeUnit
     *
     * @param \MP\UserBundle\Entity\AdministrativeUnit $administrativeUnit
     * @return Street
     */
    public function setAdministrativeUnit(\MP\UserBundle\Entity\AdministrativeUnit $administrativeUnit = null)
    {
        $this->administrativeUnit = $administrativeUnit;

        return $this;
    }

    /**
     * Get administrativeUnit
     *
     * @return \MP\UserBundle\Entity\AdministrativeUnit 
     */
    public function getAdministrativeUnit()
    {
        return $this->administrativeUnit;
    }

    /**
     * Set country
     *
     * @param \MP\UserBundle\Entity\Country $country
     * @return Street
     */
    public function setCountry(\MP\UserBundle\Entity\Country $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return \MP\UserBundle\Entity\Country 
     */
    public function getCountry()
    {
        return $this->country;
    }
}
