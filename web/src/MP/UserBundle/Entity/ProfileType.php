<?php

namespace MP\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProfileType
 */
class ProfileType
{
    /**
     * @var integer
     */
    private $profileTypeId;

    /**
     * @var integer
     */
    private $classOid;

    /**
     * @var integer
     */
    private $className;

    /**
     * @var string
     */
    private $selectorName;


    /**
     * Get profileTypeId
     *
     * @return integer 
     */
    public function getProfileTypeId()
    {
        return $this->profileTypeId;
    }

    /**
     * Set classOid
     *
     * @param integer $classOid
     * @return ProfileType
     */
    public function setClassOid($classOid)
    {
        $this->classOid = $classOid;

        return $this;
    }

    /**
     * Get classOid
     *
     * @return integer 
     */
    public function getClassOid()
    {
        return $this->classOid;
    }

    /**
     * Set className
     *
     * @param integer $className
     * @return ProfileType
     */
    public function setClassName($className)
    {
        $this->className = $className;

        return $this;
    }

    /**
     * Get className
     *
     * @return integer 
     */
    public function getClassName()
    {
        return $this->className;
    }

    /**
     * Set selectorName
     *
     * @param string $selectorName
     * @return ProfileType
     */
    public function setSelectorName($selectorName)
    {
        $this->selectorName = $selectorName;

        return $this;
    }

    /**
     * Get selectorName
     *
     * @return string 
     */
    public function getSelectorName()
    {
        return $this->selectorName;
    }
}
