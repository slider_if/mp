<?php

namespace MP\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Contact
 */
class Contact
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $contactType;

    /**
     * @var string
     */
    private $contact;

    /**
     * @var \MP\UserBundle\Entity\Personalinfo.person
     */
    private $idPerson;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set contactType
     *
     * @param integer $contactType
     * @return Contact
     */
    public function setContactType($contactType)
    {
        $this->contactType = $contactType;

        return $this;
    }

    /**
     * Get contactType
     *
     * @return integer 
     */
    public function getContactType()
    {
        return $this->contactType;
    }

    /**
     * Set contact
     *
     * @param string $contact
     * @return Contact
     */
    public function setContact($contact)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get contact
     *
     * @return string 
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set idPerson
     *
     * @param \MP\UserBundle\Entity\Personalinfo.person $idPerson
     * @return Contact
     */
    public function setIdPerson(\MP\UserBundle\Entity\Person $idPerson = null)
    {
        $this->idPerson = $idPerson;

        return $this;
    }

    /**
     * Get idPerson
     *
     * @return \MP\UserBundle\Entity\Personalinfo.person 
     */
    public function getIdPerson()
    {
        return $this->idPerson;
    }
    /**
     * @var integer
     */
    private $contactId;

    /**
     * @var integer
     */
    private $kind;

    /**
     * @var string
     */
    private $data;

    /**
     * @var integer
     */
    private $tag;

    /**
     * @var \MP\UserBundle\Entity\Person
     */
    private $person;


    /**
     * Get contactId
     *
     * @return integer 
     */
    public function getContactId()
    {
        return $this->contactId;
    }

    /**
     * Set kind
     *
     * @param integer $kind
     * @return Contact
     */
    public function setKind($kind)
    {
        $this->kind = $kind;

        return $this;
    }

    /**
     * Get kind
     *
     * @return integer 
     */
    public function getKind()
    {
        return $this->kind;
    }

    /**
     * Set data
     *
     * @param string $data
     * @return Contact
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data
     *
     * @return string 
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set tag
     *
     * @param integer $tag
     * @return Contact
     */
    public function setTag($tag)
    {
        $this->tag = $tag;

        return $this;
    }

    /**
     * Get tag
     *
     * @return integer 
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * Set person
     *
     * @param \MP\UserBundle\Entity\Person $person
     * @return Contact
     */
    public function setPerson(\MP\UserBundle\Entity\Person $person = null)
    {
        $this->person = $person;

        return $this;
    }

    /**
     * Get person
     *
     * @return \MP\UserBundle\Entity\Person 
     */
    public function getPerson()
    {
        return $this->person;
    }
}
