<?php
namespace MP\UserBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;

/**
* User
*/
class User extends BaseUser
{
public function __construct()
{
parent::__construct();
// your own logic
    $this->user_type = 1;
}
    /**
     * @var integer
     */
    protected $id;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @var \MP\UserBundle\Entity\Person
     */
    private $person;


    /**
     * Set person
     *
     * @param \MP\UserBundle\Entity\Person $person
     * @return User
     */
    public function setPerson(\MP\UserBundle\Entity\Person $person = null)
    {
        $this->person = $person;

        return $this;
    }

    /**
     * Get person
     *
     * @return \MP\UserBundle\Entity\Person
     */
    public function getPerson()
    {
        return $this->person;
    }
    /**
     * @var \MP\UserBundle\Entity\UserType
     */
    private $type;


    /**
     * Set type
     *
     * @param \MP\UserBundle\Entity\UserType $type
     * @return User
     */
    public function setType($type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \MP\UserBundle\Entity\UserType
     */
    public function getType()
    {
        return $this->type;
    }

    public function __toString()
    {
        return $this->username;
    }
    /**
     * @var string
     */
    private $user_type;

    /**
     * @var string
     */
    private $length;


    /**
     * Set user_type
     *
     * @param string $userType
     * @return User
     */
    public function setUserType($userType)
    {
        $this->user_type = $userType;

        return $this;
    }

    /**
     * Get user_type
     *
     * @return string 
     */
    public function getUserType()
    {
        return $this->user_type;
    }

    /**
     * Set length
     *
     * @param string $length
     * @return User
     */
    public function setLength($length)
    {
        $this->length = $length;

        return $this;
    }

    /**
     * Get length
     *
     * @return string 
     */
    public function getLength()
    {
        return $this->length;
    }
    /**
     * @var string
     */
    private $options;


    /**
     * Set options
     *
     * @param string $options
     * @return User
     */
    public function setOptions($options)
    {
        $this->options = $options;

        return $this;
    }

    /**
     * Get options
     *
     * @return string 
     */
    public function getOptions()
    {
        return $this->options;
    }
    /**
     * @var string
     */
    private $default;


    /**
     * Set default
     *
     * @param string $default
     * @return User
     */
    public function setDefault($default)
    {
        $this->default = $default;

        return $this;
    }

    /**
     * Get default
     *
     * @return string 
     */
    public function getDefault()
    {
        return $this->default;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $educations;


    /**
     * Add educations
     *
     * @param \MP\UserBundle\Entity\Education $educations
     * @return User
     */
    public function addEducation(\MP\UserBundle\Entity\Education $educations)
    {
        $this->educations[] = $educations;

        return $this;
    }

    /**
     * Remove educations
     *
     * @param \MP\UserBundle\Entity\Education $educations
     */
    public function removeEducation(\MP\UserBundle\Entity\Education $educations)
    {
        $this->educations->removeElement($educations);
    }

    /**
     * Get educations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEducations()
    {
        return $this->educations;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $circles;


    /**
     * Add circles
     *
     * @param \MP\UserBundle\Entity\Circle $circles
     * @return User
     */
    public function addCircle(\MP\UserBundle\Entity\Circle $circles)
    {
        $this->circles[] = $circles;

        return $this;
    }

    /**
     * Remove circles
     *
     * @param \MP\UserBundle\Entity\Circle $circles
     */
    public function removeCircle(\MP\UserBundle\Entity\Circle $circles)
    {
        $this->circles->removeElement($circles);
    }

    /**
     * Get circles
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCircles()
    {
        return $this->circles;
    }
}
