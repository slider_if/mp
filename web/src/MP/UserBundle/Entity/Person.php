<?php

namespace MP\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Person
 */
class Person
{
    /**
     * @var integer
     */
    private $id;
//
//    /**
//     * @var string
//     */
//    private $first_name;
//
//    /**
//     * @var string
//     */
//    private $last_name;
//
    /**
     * @var string
     */
    private $sur_name;
//
//    /**
//     * @var string
//     */
//    private $phone;
//
//    /**
//     * @var \DateTime
//     */
//    private $birth_date;
//
//    /**
//     * @var integer
//     */
//    private $gender;
//
    /**
     * @var \MP\UserBundle\Entity\User
     */
    private $user;
//
//    /**
//     * @var \MP\UserBundle\Entity\Appartment
//     */
//    private $address;
//
//
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
//
//    /**
//     * Set first_name
//     *
//     * @param string $firstName
//     * @return Person
//     */
//    public function setFirstName($firstName)
//    {
//        $this->first_name = $firstName;
//
//        return $this;
//    }
//
//    /**
//     * Get first_name
//     *
//     * @return string
//     */
//    public function getFirstName()
//    {
//        return $this->first_name;
//    }
//
//    /**
//     * Set last_name
//     *
//     * @param string $lastName
//     * @return Person
//     */
//    public function setLastName($lastName)
//    {
//        $this->last_name = $lastName;
//
//        return $this;
//    }
//
//    /**
//     * Get last_name
//     *
//     * @return string
//     */
//    public function getLastName()
//    {
//        return $this->last_name;
//    }
//
    /**
     * Set sur_name
     *
     * @param string $surName
     * @return Person
     */
    public function setSurName($surName)
    {
        $this->sur_name = $surName;

        return $this;
    }

    /**
     * Get sur_name
     *
     * @return string
     */
    public function getSurName()
    {
        return $this->sur_name;
    }
//
//    /**
//     * Set phone
//     *
//     * @param string $phone
//     * @return Person
//     */
//    public function setPhone($phone)
//    {
//        $this->phone = $phone;
//
//        return $this;
//    }
//
//    /**
//     * Get phone
//     *
//     * @return string
//     */
//    public function getPhone()
//    {
//        return $this->phone;
//    }
//
//    /**
//     * Set birth_date
//     *
//     * @param \DateTime $birthDate
//     * @return Person
//     */
//    public function setBirthDate($birthDate)
//    {
//        $this->birth_date = $birthDate;
//
//        return $this;
//    }
//
//    /**
//     * Get birth_date
//     *
//     * @return \DateTime
//     */
//    public function getBirthDate()
//    {
//        return $this->birth_date;
//    }
//
//    /**
//     * Set gender
//     *
//     * @param integer $gender
//     * @return Person
//     */
//    public function setGender($gender)
//    {
//        $this->gender = $gender;
//
//        return $this;
//    }
//
//    /**
//     * Get gender
//     *
//     * @return integer
//     */
//    public function getGender()
//    {
//        return $this->gender;
//    }
//
    /**
     * Set user
     *
     * @param \MP\UserBundle\Entity\User $user
     * @return Person
     */
    public function setUser(\MP\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \MP\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
//
//    /**
//     * Set address
//     *
//     * @param \MP\UserBundle\Entity\Appartment $address
//     * @return Person
//     */
//    public function setAddress(\MP\UserBundle\Entity\Appartment $address = null)
//    {
//        $this->address = $address;
//
//        return $this;
//    }
//
//    /**
//     * Get address
//     *
//     * @return \MP\UserBundle\Entity\Appartment
//     */
//    public function getAddress()
//    {
//        return $this->address;
//    }
//
    /**
     * @var \MP\UserBundle\Entity\Company
     */
    private $company;
//
//
//    /**
//     * Set company
//     *
//     * @param \MP\UserBundle\Entity\Company $company
//     * @return Person
//     */
//    public function setCompany(\MP\UserBundle\Entity\Company $company = null)
//    {
//        $this->company = $company;
//
//        return $this;
//    }
//
//    /**
//     * Get company
//     *
//     * @return \MP\UserBundle\Entity\Company
//     */
//    public function getCompany()
//    {
//        return $this->company;
//    }
    /**
     * @var string
     */
    private $lastname;

    /**
     * @var string
     */
    private $firstname;

    /**
     * @var string
     */
    private $middlename;

    /**
     * @var integer
     */
    private $sex;

    /**
     * @var \DateTime
     */
    private $birthday;


    /**
     * Set lastname
     *
     * @param string $lastname
     * @return Person
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string 
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     * @return Person
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string 
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set middlename
     *
     * @param string $middlename
     * @return Person
     */
    public function setMiddlename($middlename)
    {
        $this->middlename = $middlename;

        return $this;
    }

    /**
     * Get middlename
     *
     * @return string 
     */
    public function getMiddlename()
    {
        return $this->middlename;
    }

    /**
     * Set sex
     *
     * @param integer $sex
     * @return Person
     */
    public function setSex($sex)
    {
        $this->sex = $sex;

        return $this;
    }

    /**
     * Get sex
     *
     * @return integer 
     */
    public function getSex()
    {
        return $this->sex;
    }

    /**
     * Set birthday
     *
     * @param \DateTime $birthday
     * @return Person
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * Get birthday
     *
     * @return \DateTime 
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * Set company
     *
     * @param \MP\UserBundle\Entity\Company $company
     * @return Person
     */
    public function setCompany(\MP\UserBundle\Entity\Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \MP\UserBundle\Entity\Company 
     */
    public function getCompany()
    {
        return $this->company;
    }
    /**
     * @var \MP\UserBundle\Entity\Addr
     */
    private $address;


    /**
     * Set address
     *
     * @param \MP\UserBundle\Entity\Addr $address
     * @return Person
     */
    public function setAddress(\MP\UserBundle\Entity\Addr $address = null)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return \MP\UserBundle\Entity\Addr
     */
    public function getAddress()
    {
        return $this->address;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $educations;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->educations = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add educations
     *
     * @param \MP\UserBundle\Entity\Education $educations
     * @return Person
     */
    public function addEducation(\MP\UserBundle\Entity\Education $educations)
    {
        $this->educations[] = $educations;

        return $this;
    }

    /**
     * Remove educations
     *
     * @param \MP\UserBundle\Entity\Education $educations
     */
    public function removeEducation(\MP\UserBundle\Entity\Education $educations)
    {
        $this->educations->removeElement($educations);
    }

    /**
     * Get educations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEducations()
    {
        return $this->educations;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $circles;


    /**
     * Add circles
     *
     * @param \MP\UserBundle\Entity\Circle $circles
     * @return Person
     */
    public function addCircle(\MP\UserBundle\Entity\Circle $circles)
    {
        $this->circles[] = $circles;

        return $this;
    }

    /**
     * Remove circles
     *
     * @param \MP\UserBundle\Entity\Circle $circles
     */
    public function removeCircle(\MP\UserBundle\Entity\Circle $circles)
    {
        $this->circles->removeElement($circles);
    }

    /**
     * Get circles
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCircles()
    {
        return $this->circles;
    }
    /**
     * @var string
     */
    private $work;


    /**
     * Set work
     *
     * @param string $work
     * @return Person
     */
    public function setWork($work)
    {
        $this->work = $work;

        return $this;
    }

    /**
     * Get work
     *
     * @return string 
     */
    public function getWork()
    {
        return $this->work;
    }

    public  function __toString(){
        return $this->getFirstname();
    }
    /**
     * @var integer
     */
    private $personId;

    /**
     * @var string
     */
    private $prefix;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $suffix;

    /**
     * @var string
     */
    private $preferredname;


    /**
     * Get personId
     *
     * @return integer 
     */
    public function getPersonId()
    {
        return $this->personId;
    }

    /**
     * Set prefix
     *
     * @param string $prefix
     * @return Person
     */
    public function setPrefix($prefix)
    {
        $this->prefix = $prefix;

        return $this;
    }

    /**
     * Get prefix
     *
     * @return string 
     */
    public function getPrefix()
    {
        return $this->prefix;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Person
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set suffix
     *
     * @param string $suffix
     * @return Person
     */
    public function setSuffix($suffix)
    {
        $this->suffix = $suffix;

        return $this;
    }

    /**
     * Get suffix
     *
     * @return string 
     */
    public function getSuffix()
    {
        return $this->suffix;
    }

    /**
     * Set preferredname
     *
     * @param string $preferredname
     * @return Person
     */
    public function setPreferredname($preferredname)
    {
        $this->preferredname = $preferredname;

        return $this;
    }

    /**
     * Get preferredname
     *
     * @return string 
     */
    public function getPreferredname()
    {
        return $this->preferredname;
    }
}
