<?php

namespace MP\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CoatsuExtra
 */
class CoatsuExtra
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $codeLevel;

    /**
     * @var integer
     */
    private $codeL2sign;

    /**
     * @var integer
     */
    private $codeL3sign;

    /**
     * @var boolean
     */
    private $canModify;

    /**
     * @var \MP\UserBundle\Entity\AdministrativeUnit
     */
    private $administrativeUnit;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set codeLevel
     *
     * @param integer $codeLevel
     * @return CoatsuExtra
     */
    public function setCodeLevel($codeLevel)
    {
        $this->codeLevel = $codeLevel;

        return $this;
    }

    /**
     * Get codeLevel
     *
     * @return integer 
     */
    public function getCodeLevel()
    {
        return $this->codeLevel;
    }

    /**
     * Set codeL2sign
     *
     * @param integer $codeL2sign
     * @return CoatsuExtra
     */
    public function setCodeL2sign($codeL2sign)
    {
        $this->codeL2sign = $codeL2sign;

        return $this;
    }

    /**
     * Get codeL2sign
     *
     * @return integer 
     */
    public function getCodeL2sign()
    {
        return $this->codeL2sign;
    }

    /**
     * Set codeL3sign
     *
     * @param integer $codeL3sign
     * @return CoatsuExtra
     */
    public function setCodeL3sign($codeL3sign)
    {
        $this->codeL3sign = $codeL3sign;

        return $this;
    }

    /**
     * Get codeL3sign
     *
     * @return integer 
     */
    public function getCodeL3sign()
    {
        return $this->codeL3sign;
    }

    /**
     * Set canModify
     *
     * @param boolean $canModify
     * @return CoatsuExtra
     */
    public function setCanModify($canModify)
    {
        $this->canModify = $canModify;

        return $this;
    }

    /**
     * Get canModify
     *
     * @return boolean 
     */
    public function getCanModify()
    {
        return $this->canModify;
    }

    /**
     * Set administrativeUnit
     *
     * @param \MP\UserBundle\Entity\AdministrativeUnit $administrativeUnit
     * @return CoatsuExtra
     */
    public function setAdministrativeUnit(\MP\UserBundle\Entity\AdministrativeUnit $administrativeUnit = null)
    {
        $this->administrativeUnit = $administrativeUnit;

        return $this;
    }

    /**
     * Get administrativeUnit
     *
     * @return \MP\UserBundle\Entity\AdministrativeUnit 
     */
    public function getAdministrativeUnit()
    {
        return $this->administrativeUnit;
    }
}
