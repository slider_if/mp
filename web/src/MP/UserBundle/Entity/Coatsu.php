<?php

namespace MP\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Coatsu
 */
class Coatsu
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $code;

    /**
     * @var string
     */
    private $unittype;

    /**
     * @var string
     */
    private $unitname;

    /**
     * @var integer
     */
    private $codelevel;

    /**
     * @var integer
     */
    private $codel2sign;

    /**
     * @var integer
     */
    private $codel3sign;

    /**
     * @var \DateTime
     */
    private $modified;

    /**
     * @var string
     */
    private $comment;

    /**
     * @var \MP\UserBundle\Entity\Common.Coatsu
     */
    private $idParent;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return Coatsu
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set unittype
     *
     * @param string $unittype
     * @return Coatsu
     */
    public function setUnittype($unittype)
    {
        $this->unittype = $unittype;

        return $this;
    }

    /**
     * Get unittype
     *
     * @return string 
     */
    public function getUnittype()
    {
        return $this->unittype;
    }

    /**
     * Set unitname
     *
     * @param string $unitname
     * @return Coatsu
     */
    public function setUnitname($unitname)
    {
        $this->unitname = $unitname;

        return $this;
    }

    /**
     * Get unitname
     *
     * @return string 
     */
    public function getUnitname()
    {
        return $this->unitname;
    }

    /**
     * Set codelevel
     *
     * @param integer $codelevel
     * @return Coatsu
     */
    public function setCodelevel($codelevel)
    {
        $this->codelevel = $codelevel;

        return $this;
    }

    /**
     * Get codelevel
     *
     * @return integer 
     */
    public function getCodelevel()
    {
        return $this->codelevel;
    }

    /**
     * Set codel2sign
     *
     * @param integer $codel2sign
     * @return Coatsu
     */
    public function setCodel2sign($codel2sign)
    {
        $this->codel2sign = $codel2sign;

        return $this;
    }

    /**
     * Get codel2sign
     *
     * @return integer 
     */
    public function getCodel2sign()
    {
        return $this->codel2sign;
    }

    /**
     * Set codel3sign
     *
     * @param integer $codel3sign
     * @return Coatsu
     */
    public function setCodel3sign($codel3sign)
    {
        $this->codel3sign = $codel3sign;

        return $this;
    }

    /**
     * Get codel3sign
     *
     * @return integer 
     */
    public function getCodel3sign()
    {
        return $this->codel3sign;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return Coatsu
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return Coatsu
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set idParent
     *
     * @param \MP\UserBundle\Entity\Common.Coatsu $idParent
     * @return Coatsu
     */
    public function setIdParent(\MP\UserBundle\Entity\Coatsu $idParent = null)
    {
        $this->idParent = $idParent;

        return $this;
    }

    /**
     * Get idParent
     *
     * @return \MP\UserBundle\Entity\Common.Coatsu 
     */
    public function getIdParent()
    {
        return $this->idParent;
    }

    public  function __toString(){
      return ($this->getUnitname() == '' ? 'Null' : $this->getUnitname());
//        return $this->getUnitname();

    }
}
