<?php

namespace MP\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PersonType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('lastname', null,
    array('label' => 'person.last_name'))
            ->add('firstname', null,
            array('label' => 'person.first_name'))
            ->add('sur_name', null,
            array('label' => 'person.sur_name'))
            ->add('sex', null,
            array('label' => 'person.sex'))
            ->add('birthday', null,
            array('label' => 'person.birthday'))
            ->add('work', null,
            array('label' => 'person.work'))
//            ->add('user')
//            ->add('company')
            ->add('address', null,
            array('label' => 'person.address'))
//            ->add('educations')
//            ->add('circles')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MP\UserBundle\Entity\Person'
        ));
        $resolver->setDefaults(array(
            'translation_domain' => 'MPUserBundle'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'mp_userbundle_person';
    }
}
