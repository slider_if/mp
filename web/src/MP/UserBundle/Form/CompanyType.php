<?php

namespace MP\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CompanyType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('company_name', null,
            array('label' => 'company.company_name'))
            ->add('contact_name', null,
            array('label' => 'company.contact_name'))
            ->add('phone', null,
            array('label' => 'company.phone'))
            ->add('about', null,
            array('label' => 'company.about'))
            ->add('work_type', null,
            array('label' => 'company.work_type'))
            ->add('foundation_year', null,
            array('label' => 'company.foundation_year'))
            ->add('opf', null,
            array('label' => 'company.opf'))
            ->add('uf', null,
            array('label' => 'company.uf'))
            ->add('stuff', null,
            array('label' => 'company.stuff'))
            ->add('brends', null,
            array('label' => 'company.brends'))
            ->add('clients', null,
            array('label' => 'company.clients'))
            ->add('sales_a_year', null,
            array('label' => 'company.sales_a_year'))
            ->add('certificates', null,
            array('label' => 'company.certificates'))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MP\UserBundle\Entity\Company'
        ));
        $resolver->setDefaults(array(
            'translation_domain' => 'MPUserBundle'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'mp_userbundle_company';
    }
}
