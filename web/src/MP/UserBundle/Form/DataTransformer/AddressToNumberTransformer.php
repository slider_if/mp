<?php
namespace MP\UserBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Doctrine\Common\Persistence\ObjectManager;
use MP\MPUserBundle\Entity\User;

class AddressToNumberTransformer implements DataTransformerInterface
{
    /**
     * @var ObjectManager
     */
    private $om;

    /**
     * @param ObjectManager $om
     */
    public function __construct(ObjectManager $om)
    {
        $this->om = $om;
    }

    /**
     * Transforms an object (issue) to a string (number).
     *
     * @param
     * @return string
     */
    public function transform($address)
    {
        if (null === $address) {
            return "";
        }

        return $address->getId();
    }

    /**
     * Transforms a string (number) to an object (issue).
     *
     * @param  string $number
     *
     * @return User|null
     *
     * @throws TransformationFailedException if object (issue) is not found.
     */
    public function reverseTransform($number)
    {
        if (!$number) {
            return null;
        }

        $issue = $this->om
            ->getRepository('MPUserBundle:Addr')
            ->find($number)
        ;

        if (null === $issue) {
            throw new TransformationFailedException(sprintf(
                'An address with number "%s" does not exist!',
                $number
            ));
        }

        return $issue;
    }
}