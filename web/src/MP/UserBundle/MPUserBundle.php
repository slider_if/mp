<?php

namespace MP\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class MPUserBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
