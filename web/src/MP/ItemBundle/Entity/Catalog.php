<?php

namespace MP\ItemBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Catalog
 */
class Catalog
{
    /**
     * @var integer
     */
    private $scps_id;

    /**
     * @var string
     */
    private $code;

    /**
     * @var string
     */
    private $grade;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $ucpfea_codes;

    /**
     * @var \DateTime
     */
    private $modified;

    /**
     * @var string
     */
    private $comment;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $children;

    /**
     * @var \MP\ItemBundle\Entity\Catalog
     */
    private $parent;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->children = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get scps_id
     *
     * @return integer 
     */
    public function getScpsId()
    {
        return $this->scps_id;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return Catalog
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set grade
     *
     * @param string $grade
     * @return Catalog
     */
    public function setGrade($grade)
    {
        $this->grade = $grade;

        return $this;
    }

    /**
     * Get grade
     *
     * @return string 
     */
    public function getGrade()
    {
        return $this->grade;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Catalog
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set ucpfea_codes
     *
     * @param string $ucpfeaCodes
     * @return Catalog
     */
    public function setUcpfeaCodes($ucpfeaCodes)
    {
        $this->ucpfea_codes = $ucpfeaCodes;

        return $this;
    }

    /**
     * Get ucpfea_codes
     *
     * @return string 
     */
    public function getUcpfeaCodes()
    {
        return $this->ucpfea_codes;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return Catalog
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return Catalog
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Add children
     *
     * @param \MP\ItemBundle\Entity\Catalog $children
     * @return Catalog
     */
    public function addChild(\MP\ItemBundle\Entity\Catalog $children)
    {
        $this->children[] = $children;

        return $this;
    }

    /**
     * Remove children
     *
     * @param \MP\ItemBundle\Entity\Catalog $children
     */
    public function removeChild(\MP\ItemBundle\Entity\Catalog $children)
    {
        $this->children->removeElement($children);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Set parent
     *
     * @param \MP\ItemBundle\Entity\Catalog $parent
     * @return Catalog
     */
    public function setParent(\MP\ItemBundle\Entity\Catalog $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \MP\ItemBundle\Entity\Catalog 
     */
    public function getParent()
    {
        return $this->parent;
    }
}
