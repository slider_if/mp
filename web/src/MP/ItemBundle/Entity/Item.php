<?php

namespace MP\ItemBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Item
 */
class Item
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $model;

    /**
     * @var string
     */
    private $manufactor;

    /**
     * @var integer
     */
    private $unit;

    /**
     * @var string
     */
    private $main_picture;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $orders;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $proposals;

    /**
     * @var \MP\UserBundle\Entity\User
     */
    private $user;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->orders = new \Doctrine\Common\Collections\ArrayCollection();
        $this->proposals = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set model
     *
     * @param string $model
     * @return Item
     */
    public function setModel($model)
    {
        $this->model = $model;

        return $this;
    }

    /**
     * Get model
     *
     * @return string 
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Set manufactor
     *
     * @param string $manufactor
     * @return Item
     */
    public function setManufactor($manufactor)
    {
        $this->manufactor = $manufactor;

        return $this;
    }

    /**
     * Get manufactor
     *
     * @return string 
     */
    public function getManufactor()
    {
        return $this->manufactor;
    }

    /**
     * Set unit
     *
     * @param integer $unit
     * @return Item
     */
    public function setUnit($unit)
    {
        $this->unit = $unit;

        return $this;
    }

    /**
     * Get unit
     *
     * @return integer 
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * Set main_picture
     *
     * @param string $mainPicture
     * @return Item
     */
    public function setMainPicture($mainPicture)
    {
        $this->main_picture = $mainPicture;

        return $this;
    }

    /**
     * Get main_picture
     *
     * @return string 
     */
    public function getMainPicture()
    {
        return $this->main_picture;
    }

    /**
     * Add orders
     *
     * @param \MP\ItemBundle\Entity\Order $orders
     * @return Item
     */
    public function addOrder(\MP\ItemBundle\Entity\Order $orders)
    {
        $this->orders[] = $orders;

        return $this;
    }

    /**
     * Remove orders
     *
     * @param \MP\ItemBundle\Entity\Order $orders
     */
    public function removeOrder(\MP\ItemBundle\Entity\Order $orders)
    {
        $this->orders->removeElement($orders);
    }

    /**
     * Get orders
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getOrders()
    {
        return $this->orders;
    }

    /**
     * Add proposals
     *
     * @param \MP\ItemBundle\Entity\Proposal $proposals
     * @return Item
     */
    public function addProposal(\MP\ItemBundle\Entity\Proposal $proposals)
    {
        $this->proposals[] = $proposals;

        return $this;
    }

    /**
     * Remove proposals
     *
     * @param \MP\ItemBundle\Entity\Proposal $proposals
     */
    public function removeProposal(\MP\ItemBundle\Entity\Proposal $proposals)
    {
        $this->proposals->removeElement($proposals);
    }

    /**
     * Get proposals
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProposals()
    {
        return $this->proposals;
    }

    /**
     * Set user
     *
     * @param \MP\UserBundle\Entity\User $user
     * @return Item
     */
    public function setUser(\MP\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \MP\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}
