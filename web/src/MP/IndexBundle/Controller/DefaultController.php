<?php

namespace MP\IndexBundle\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
    	$content = $this->render('IndexBundle:Default:index.html.twig', array('isServ' => false));

    //$this->getCatalog();
   //  	if( $this->getRequest()->isXMLHttpRequest() )
   //  	{
   //  		$response = new JsonResponse();
			// $response->setData(array(
			//     'html' => $content
			// ));

	  //       return $response;
   //  	} 
   //  	else
   //  	{
    		return $content;
    	//}
    }

    public function indexServAction()
    {
      $content = $this->render('IndexBundle:Default:index.html.twig', array('isServ' => true));

      return $content;
    }

    private function getCatalog() {
      $em = $this->getDoctrine()->getManager();

      $repository = $em->getRepository('MPItemBundle:Catalog');

      $file = fopen('/var/www/dk/web/web/bundles/dk/js/libs4.js', 'w');


      $this->allCatalog($repository, $file);
      die();
    }

    private function allCatalog($repository, $file, $parenId = false, $level = 0) {
      $query = $repository->createQueryBuilder('c')
            ->select('c.scps_id,c.title AS value')
            ->orderBy('c.title', 'ASC');
      if($parenId ) {
        $query->where('c.parent = :parntId')
                ->setParameter('parntId', $parenId);
      } else {
        $query
          //->where('c.parent IS NULL AND (c.code = :A OR c.code = :B OR c.code = :C)')
          ->where('c.parent IS NULL AND c.code <> :A AND c.code <> :B AND c.code <> :C')
          ->setParameter('A', 'A')
          ->setParameter('B', 'B')
          ->setParameter('C', 'C');
      }

      $query = $query->getQuery();

      $entities = $query->getResult();

      if(count($entities) > 0) {
        if($level == 0) {
          //fwrite($file, "var catalgData = ") ;
          fwrite($file, "var catalgServData = ") ;
        } else {
          fwrite($file, ",children:");
        }
        fwrite($file, "[");
      }

      foreach ($entities as $key => $value) {
        fwrite($file, '{label:"');
        fwrite($file, htmlspecialchars($value['value']).'"');
        if($level <=3  ) {
          $this->allCatalog($repository, $file, $value['scps_id'], $level+1);
        }
        fwrite($file, "},");
      }

      if(count($entities) > 0) {
        fwrite($file, "]");
      }

    }
}