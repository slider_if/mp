<?php

namespace MP\IndexBundle\Controller;

use MP\UserBundle\Entity\Circle;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AddressController extends Controller
{
    public function getSelectAction($id = false, $simple = false)
    {
    //     $em = $this->getDoctrine()->getManager();

    //     $repository = $em->getRepository('MPUserBundle:Coatsu');

    //     $query = $repository->createQueryBuilder('c')
    //         ->select('c.id,c.unitname AS value')
    //         //->where('c.unitname <> ""')
    //         ->orderBy('c.unitname', 'ASC');

    //     if($id) {
    //         $query->where('c.idParent = :parntId')
    //             ->setParameter('parntId', $id);
    //     } else {
    //         $query->where('c.idParent IS NULL');
    //     }

    //     $query = $query->getQuery();

    //     $entities = $query->getResult();
    //     $user= $this->get('security.context')->getToken()->getUser();
    //     if ($id){
    //         $lastCoatsu = $this->getDoctrine()
    //             ->getRepository('MPUserBundle:Coatsu')
    //             ->find($id);
    //         $circle = $this->getDoctrine()
    //             ->getRepository('MPUserBundle:Circle')
    //             ->findByCoatsu($lastCoatsu);
    //         if(count($circle) == 0 && $lastCoatsu->getUnitname() != '') {
    //             $newCircle = new Circle;
    //             $newCircle->addUser($user);
    //             $newCircle->setDescription($lastCoatsu->getUnitname());
    //             $newCircle->setCoatsu($lastCoatsu);
    //             $newCircle->setTitle($lastCoatsu->getUnitname());
    //             $newCircle->setType(0);
    //             $em->persist($newCircle);
    //             $em->flush();

    //         }else{
    //             $checkUsers = $circle[0]->getUsers();
    //             $userInCircle = false;
    //             foreach ($checkUsers as $checkUser){
    //                 if($checkUser == $user){
    //                     $userInCircle = true;
    //                 }
    //             }
    //             if(!$userInCircle){
    //                 $circle[0]->addUser($user);
    //                 $em->flush();
    //             }
    //         }
    //     }

    //     $isLast = false;
    //     if(count($entities) == 0 && $id > 0) {
    //         $lastCoatsu = $this->getDoctrine()
    //             ->getRepository('MPUserBundle:Coatsu')
    //             ->find($id);

    //         $coatsuCode = $lastCoatsu->getCode();

    //         $circle = $this->getDoctrine()
    //             ->getRepository('MPUserBundle:Circle')
    //             ->findByCoatsu($lastCoatsu);
    //         if(count($circle) == 0 && $lastCoatsu->getUnitname() != '') {
    //             $newCircle = new Circle;
    //             $newCircle->addUser($user);
    //             $newCircle->setDescription($lastCoatsu->getUnitname());
    //             $newCircle->setCoatsu($lastCoatsu);
    //             $newCircle->setTitle($lastCoatsu->getUnitname());
    //             $newCircle->setType(0);
    //             $em->persist($newCircle);
    //             $em->flush();

    //         }else{
    //             $checkUsers = $circle[0]->getUsers();
    //             $userInCircle = false;
    //             foreach ($checkUsers as $checkUser){
    //                 if($checkUser == $user){
    //                     $userInCircle = true;
    //                 }
    //             }
    //             if(!$userInCircle){
    //                 $circle[0]->addUser($user);
    //                 $em->flush();
    //             }
    //         }
    //         $repository = $em->getRepository('MPUserBundle:Addr');

    //         $query = $repository->createQueryBuilder('a')
    //             ->select('a.id,a.data AS value')
    //             ->where('a.koatuu = :parntId')
    //             ->setParameter('parntId', $coatsuCode)
    //             ->orderBy('a.name', 'ASC');

    //         $query = $query->getQuery();

    //         $entities = $query->getResult();

    //         foreach ($entities as $key => $value) {
    //             $val = substr($value['value'], 2, strlen($value['value'])-4);
    //             $val = explode('","', $val);
    //             unset($val[0], $val[1]);
    //             $entities[$key]['value'] = implode(', ', $val);
    //         }

    //         $isLast = true;
    //     }

    //     return $this->render('IndexBundle:Address:getSelect.html.twig', array(
    //             'entities' => $entities,
    //             'url' => $this->generateUrl('get_select'),
    //             'simple' => $simple,
    //             'isLast' => $isLast
    //         ));    
    // }

    // public function getSelectAjaxAction($id = false)
    // {
    //     return $this->getSelectAction($id, true);
    // }

    // public function getSelectsAction()
    // {
    //     return $this->render('IndexBundle:Address:getSelects.html.twig', array(
    //             // ...
    //         ));    
    }

}
