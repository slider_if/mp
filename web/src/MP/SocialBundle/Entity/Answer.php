<?php

namespace MP\SocialBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Answer
 */
class Answer
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $message;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $rating_answers;

    /**
     * @var \MP\SocialBundle\Entity\Question
     */
    private $question;

    /**
     * @var \MP\UserBundle\Entity\User
     */
    private $user;
    private $rating;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->rating_answers = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set message
     *
     * @param string $message
     * @return Answer
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string 
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Add rating_answers
     *
     * @param \MP\SocialBundle\Entity\RatingAnswer $ratingAnswers
     * @return Answer
     */
    public function addRatingAnswer(\MP\SocialBundle\Entity\RatingAnswer $ratingAnswers)
    {
        $this->rating_answers[] = $ratingAnswers;

        return $this;
    }

    /**
     * Remove rating_answers
     *
     * @param \MP\SocialBundle\Entity\RatingAnswer $ratingAnswers
     */
    public function removeRatingAnswer(\MP\SocialBundle\Entity\RatingAnswer $ratingAnswers)
    {
        $this->rating_answers->removeElement($ratingAnswers);
    }

    /**
     * Get rating_answers
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRatingAnswers()
    {
        return $this->rating_answers;
    }

    /**
     * Set question
     *
     * @param \MP\SocialBundle\Entity\Question $question
     * @return Answer
     */
    public function setQuestion(\MP\SocialBundle\Entity\Question $question = null)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question
     *
     * @return \MP\SocialBundle\Entity\Question 
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Set user
     *
     * @param \MP\UserBundle\Entity\User $user
     * @return Answer
     */
    public function setUser(\MP\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \MP\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    public function __toString()
    {
        return $this->message;
    }

    public function countRating(){
        $finalRating = 0;
        $ratingCount = (count($this->rating_answers) > 0 ? count($this->rating_answers) : 1);

        foreach ($this->rating_answers as $rating){
            $finalRating = $finalRating + $rating->getRating();
        }

        $finalRating = $finalRating / $ratingCount;

        return $finalRating;
    }

    public function getRating(){
        return $this->countRating();
    }
}
