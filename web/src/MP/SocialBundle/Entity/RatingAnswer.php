<?php

namespace MP\SocialBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RatingAnswer
 */
class RatingAnswer
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $rating;

    /**
     * @var \MP\SocialBundle\Entity\Answer
     */
    private $discussion;

    /**
     * @var \MP\UserBundle\Entity\User
     */
    private $user;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set rating
     *
     * @param integer $rating
     * @return RatingAnswer
     */
    public function setRating($rating)
    {
        $this->rating = $rating;

        return $this;
    }

    /**
     * Get rating
     *
     * @return integer 
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * Set discussion
     *
     * @param \MP\SocialBundle\Entity\Answer $discussion
     * @return RatingAnswer
     */
    public function setDiscussion(\MP\SocialBundle\Entity\Answer $discussion = null)
    {
        $this->discussion = $discussion;

        return $this;
    }

    /**
     * Get discussion
     *
     * @return \MP\SocialBundle\Entity\Answer 
     */
    public function getDiscussion()
    {
        return $this->discussion;
    }

    /**
     * Set user
     *
     * @param \MP\UserBundle\Entity\User $user
     * @return RatingAnswer
     */
    public function setUser(\MP\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \MP\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    public function __toString()
    {
        return strval($this->rating);
    }
    /**
     * @var \MP\SocialBundle\Entity\Answer
     */
    private $answer;


    /**
     * Set answer
     *
     * @param \MP\SocialBundle\Entity\Answer $answer
     * @return RatingAnswer
     */
    public function setAnswer(\MP\SocialBundle\Entity\Answer $answer = null)
    {
        $this->answer = $answer;

        return $this;
    }

    /**
     * Get answer
     *
     * @return \MP\SocialBundle\Entity\Answer 
     */
    public function getAnswer()
    {
        return $this->answer;
    }
}
