<?php

namespace MP\SocialBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RatingQuestion
 */
class RatingQuestion
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $rating;

    /**
     * @var \MP\SocialBundle\Entity\Question
     */
    private $discussion;

    /**
     * @var \MP\UserBundle\Entity\User
     */
    private $user;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set rating
     *
     * @param integer $rating
     * @return RatingQuestion
     */
    public function setRating($rating)
    {
        $this->rating = $rating;

        return $this;
    }

    /**
     * Get rating
     *
     * @return integer 
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * Set discussion
     *
     * @param \MP\SocialBundle\Entity\Question $discussion
     * @return RatingQuestion
     */
    public function setDiscussion(\MP\SocialBundle\Entity\Question $discussion = null)
    {
        $this->discussion = $discussion;

        return $this;
    }

    /**
     * Get discussion
     *
     * @return \MP\SocialBundle\Entity\Question 
     */
    public function getDiscussion()
    {
        return $this->discussion;
    }

    /**
     * Set user
     *
     * @param \MP\UserBundle\Entity\User $user
     * @return RatingQuestion
     */
    public function setUser(\MP\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \MP\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    public function __toString()
    {
        return $this->rating;
    }
    /**
     * @var \MP\SocialBundle\Entity\Question
     */
    private $question;


    /**
     * Set question
     *
     * @param \MP\SocialBundle\Entity\Question $question
     * @return RatingQuestion
     */
    public function setQuestion(\MP\SocialBundle\Entity\Question $question = null)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question
     *
     * @return \MP\SocialBundle\Entity\Question 
     */
    public function getQuestion()
    {
        return $this->question;
    }
}
