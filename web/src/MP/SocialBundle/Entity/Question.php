<?php

namespace MP\SocialBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Question
 */
class Question
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $message;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $answers;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $rating_questions;

    /**
     * @var \MP\SocialBundle\Entity\Discussion
     */
    private $discussion;

    /**
     * @var \MP\UserBundle\Entity\User
     */
    private $user;

    private $rating;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->answers = new \Doctrine\Common\Collections\ArrayCollection();
        $this->rating_questions = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set message
     *
     * @param string $message
     * @return Question
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string 
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Add answers
     *
     * @param \MP\SocialBundle\Entity\Answer $answers
     * @return Question
     */
    public function addAnswer(\MP\SocialBundle\Entity\Answer $answers)
    {
        $this->answers[] = $answers;

        return $this;
    }

    /**
     * Remove answers
     *
     * @param \MP\SocialBundle\Entity\Answer $answers
     */
    public function removeAnswer(\MP\SocialBundle\Entity\Answer $answers)
    {
        $this->answers->removeElement($answers);
    }

    /**
     * Get answers
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAnswers()
    {
        return $this->answers;
    }

    /**
     * Add rating_questions
     *
     * @param \MP\SocialBundle\Entity\RatingQuestion $ratingQuestions
     * @return Question
     */
    public function addRatingQuestion(\MP\SocialBundle\Entity\RatingQuestion $ratingQuestions)
    {
        $this->rating_questions[] = $ratingQuestions;

        return $this;
    }

    /**
     * Remove rating_questions
     *
     * @param \MP\SocialBundle\Entity\RatingQuestion $ratingQuestions
     */
    public function removeRatingQuestion(\MP\SocialBundle\Entity\RatingQuestion $ratingQuestions)
    {
        $this->rating_questions->removeElement($ratingQuestions);
    }

    /**
     * Get rating_questions
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRatingQuestions()
    {
        return $this->rating_questions;
    }


    /**
     * Set user
     *
     * @param \MP\UserBundle\Entity\User $user
     * @return Question
     */
    public function setUser(\MP\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \MP\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
    /**
     * @var \MP\UserBundle\Entity\User
     */
    private $target;


    /**
     * Set target
     *
     * @param \MP\UserBundle\Entity\User $target
     * @return Question
     */
    public function setTarget(\MP\UserBundle\Entity\User $target = null)
    {
        $this->target = $target;

        return $this;
    }

    /**
     * Get target
     *
     * @return \MP\UserBundle\Entity\User 
     */
    public function getTarget()
    {
        return $this->target;
    }

    public function __toString()
    {
        return $this->message;
    }

    public function countRating(){
        $finalRating = 0;
        $ratingCount = (count($this->rating_questions) > 0 ? count($this->rating_questions) : 1);

        foreach ($this->rating_questions as $rating){
            $finalRating = $finalRating + $rating->getRating();
        }

        $finalRating = $finalRating / $ratingCount;

        return $finalRating;
    }

    public function getRating(){
        return $this->countRating();
    }
    /**
     * @var \MP\UserBundle\Entity\Circle
     */
    private $circle;


    /**
     * Set circle
     *
     * @param \MP\UserBundle\Entity\Circle $circle
     * @return Question
     */
    public function setCircle(\MP\UserBundle\Entity\Circle $circle = null)
    {
        $this->circle = $circle;

        return $this;
    }

    /**
     * Get circle
     *
     * @return \MP\UserBundle\Entity\Circle 
     */
    public function getCircle()
    {
        return $this->circle;
    }
}
