<?php

namespace MP\SocialBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use MP\SocialBundle\Entity\RatingQuestion;
use MP\SocialBundle\Form\RatingQuestionType;

/**
 * RatingQuestion controller.
 *
 */
class RatingQuestionController extends Controller
{

    /**
     * Lists all RatingQuestion entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MPSocialBundle:RatingQuestion')->findAll();

        return $this->render('MPSocialBundle:RatingQuestion:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new RatingQuestion entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new RatingQuestion();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('question_show', array('id' => $entity->getQuestion()->getId())));
        }

        return $this->render('MPSocialBundle:RatingQuestion:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a RatingQuestion entity.
     *
     * @param RatingQuestion $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(RatingQuestion $entity)
    {
        $form = $this->createForm(new RatingQuestionType(), $entity, array(
            'action' => $this->generateUrl('ratingquestion_create'),
            'method' => 'POST',
            'em' => $this->getDoctrine()->getManager()
        ));

        $form->add('submit', 'submit', array('label' => 'Оцінити'));

        return $form;
    }

    /**
     * Displays a form to create a new RatingQuestion entity.
     *
     */
    public function newAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.context')->getToken()->getUser();
        $question = $em->getRepository('MPSocialBundle:Question')->find($id);
        $entity = new RatingQuestion();
        $entity->setUser($user);
        $entity->setQuestion($question);
        $form   = $this->createCreateForm($entity);

        return $this->render('MPSocialBundle:RatingQuestion:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a RatingQuestion entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MPSocialBundle:RatingQuestion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find RatingQuestion entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MPSocialBundle:RatingQuestion:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing RatingQuestion entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MPSocialBundle:RatingQuestion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find RatingQuestion entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MPSocialBundle:RatingQuestion:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a RatingQuestion entity.
    *
    * @param RatingQuestion $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(RatingQuestion $entity)
    {
        $form = $this->createForm(new RatingQuestionType(), $entity, array(
            'action' => $this->generateUrl('ratingquestion_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing RatingQuestion entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MPSocialBundle:RatingQuestion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find RatingQuestion entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('ratingquestion_edit', array('id' => $id)));
        }

        return $this->render('MPSocialBundle:RatingQuestion:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a RatingQuestion entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('MPSocialBundle:RatingQuestion')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find RatingQuestion entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('ratingquestion'));
    }

    /**
     * Creates a form to delete a RatingQuestion entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('ratingquestion_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
