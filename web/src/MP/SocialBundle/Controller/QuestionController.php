<?php

namespace MP\SocialBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use MP\SocialBundle\Entity\Question;
use MP\SocialBundle\Form\QuestionType;
use Doctrine\ORM\Query\ResultSetMapping;

/**
 * Question controller.
 *
 */
class QuestionController extends Controller
{

    /**
     * Lists all Question entities.
     *
     */
    public function indexAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('MPUserBundle:User')->findOneBy(array('id' => $id));
//        $em = $this->getDoctrine()->getManager();
//        $user = $this->get('security.context')->getToken()->getUser();

        $entities = $em->getRepository('MPSocialBundle:Question')->findByTarget($user->getId());

        return $this->render('MPSocialBundle:Question:index.html.twig', array(
            'entities' => $entities,
        ));
        $user = $this->get('security.context')->getToken()->getUser();
    }


    /**
     * Lists all Question entities with user.
     *
     */
    public function questionsWithUserAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('MPUserBundle:User')->findOneBy(array('id' => $id));
        $em = $this->getDoctrine()->getManager();
        $rsm = new ResultSetMapping;
        $rsm->addEntityResult('MPSocialBundle:Question', 'question');
        $rsm->addFieldResult('question', 'id', 'id');
        $rsm->addMetaResult('question','user_id','user_id');
        $rsm->addMetaResult('question','target_id','target_id');
        $rsm->addFieldResult('question','message','message');
        $rsm->addMetaResult('question','circle_id','circle_id');
        $sql = 'SELECT DISTINCT question.id, question.user_id, question.target_id, question.message, question.circle_id FROM question LEFT JOIN answer ON (question.id = answer.question_id) LEFT JOIN rating_answer ON(answer.id = rating_answer.answer_id) LEFT JOIN rating_question ON(question.id = rating_question.question_id) WHERE question.target_id = :id OR question.user_id = :id OR answer.user_id = :id OR rating_answer.user_id = :id OR rating_question.user_id = :id';
        $query = $em->createNativeQuery($sql, $rsm);
        $query->setParameters(array('id'=>$user->getId()));
        $entities = $query->getResult();

        return $this->render('MPSocialBundle:Question:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * Lists all personal Question entities.
     *
     */
    public function indexPersonalAction()
    {
        $user = $this->get('security.context')->getToken()->getUser();

        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MPSocialBundle:Question')->find(array('user_id' => $user->getId()));

        return $this->render('MPSocialBundle:Question:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * Creates a new Question entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Question();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('question_show', array('id' => $entity->getId())));
        }

        return $this->render('MPSocialBundle:Question:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    public function createForCircleAction(Request $request){
        $id = $request->get('id');
        $em = $this->getDoctrine()->getManager();
        $circle = $em->getRepository('MPUserBundle:Circle')->findOneBy(array('id' => $id));
        $user = $this->get('security.context')->getToken()->getUser();

        $entity = new Question();
        $entity->setUser($user);
        $entity->setTarget($user);
        $entity->setCircle($circle);
        $form   = $this->createCreateForm($entity);

        return $this->render('MPSocialBundle:Question:new_circle.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'id'     => $id
        ));
    }

    /**
     * Creates a form to create a Question entity.
     *
     * @param Question $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Question $entity)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new QuestionType(), $entity, array(
            'action' => $this->generateUrl('question_create'),
            'method' => 'POST',
            'em' => $em,
        ));

        $form->add('submit', 'submit', array('label' => 'Створити обговорення'));

        return $form;
    }

    /**
     * Displays a form to create a new Question entity.
     *
     */
    public function newAction()
    {
        $user = $this->get('security.context')->getToken()->getUser();

        $entity = new Question();
        $entity->setUser($user);
        $entity->setTarget($user);
        $form   = $this->createCreateForm($entity);

        return $this->render('MPSocialBundle:Question:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'id'     => 0
        ));
    }


    /**
     * Displays a form to create a new Question entity.
     *
     */
    public function newPersonalAction($id)
    {
        $entity = new Question();
        $userLogged = $this->get('security.context')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('MPUserBundle:User')->findOneBy(array('id' => $id));
        $entity->setTarget($user);
        $entity->setUser($userLogged);
        $form   = $this->createCreateForm($entity);
        $userLogged = $this->get('security.context')->getToken()->getUser();
        return $this->render('MPSocialBundle:Question:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'id' => $id,
        ));
    }

    /**
     * Finds and displays a Question entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MPSocialBundle:Question')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Question entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MPSocialBundle:Question:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Question entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MPSocialBundle:Question')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Question entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MPSocialBundle:Question:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Question entity.
    *
    * @param Question $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Question $entity)
    {
        $form = $this->createForm(new QuestionType(), $entity, array(
            'action' => $this->generateUrl('question_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Question entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MPSocialBundle:Question')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Question entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('question_edit', array('id' => $id)));
        }

        return $this->render('MPSocialBundle:Question:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Question entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('MPSocialBundle:Question')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Question entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('question'));
    }

    /**
     * Creates a form to delete a Question entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('question_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
