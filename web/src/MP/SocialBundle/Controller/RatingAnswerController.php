<?php

namespace MP\SocialBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use MP\SocialBundle\Entity\RatingAnswer;
use MP\SocialBundle\Form\RatingAnswerType;

/**
 * RatingAnswer controller.
 *
 */
class RatingAnswerController extends Controller
{

    /**
     * Lists all RatingAnswer entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MPSocialBundle:RatingAnswer')->findAll();

        return $this->render('MPSocialBundle:RatingAnswer:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new RatingAnswer entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new RatingAnswer();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('question_show', array('id' => $entity->getAnswer()->getQuestion()->getId())));
        }

        return $this->render('MPSocialBundle:RatingAnswer:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a RatingAnswer entity.
     *
     * @param RatingAnswer $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(RatingAnswer $entity)
    {
        $form = $this->createForm(new RatingAnswerType(), $entity, array(
            'action' => $this->generateUrl('ratinganswer_create'),
            'method' => 'POST',
            'em' => $this->getDoctrine()->getManager(),
        ));

        $form->add('submit', 'submit', array('label' => 'Оцінити'));

        return $form;
    }

    /**
     * Displays a form to create a new RatingAnswer entity.
     *
     */
    public function newAction($id)
    {

        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.context')->getToken()->getUser();
        $answer = $em->getRepository('MPSocialBundle:Answer')->find($id);
        $entity = new RatingAnswer();
        $entity->setUser($user);
        $entity->setAnswer($answer);
        $form   = $this->createCreateForm($entity);

        return $this->render('MPSocialBundle:RatingAnswer:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a RatingAnswer entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MPSocialBundle:RatingAnswer')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find RatingAnswer entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MPSocialBundle:RatingAnswer:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing RatingAnswer entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MPSocialBundle:RatingAnswer')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find RatingAnswer entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MPSocialBundle:RatingAnswer:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a RatingAnswer entity.
    *
    * @param RatingAnswer $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(RatingAnswer $entity)
    {
        $form = $this->createForm(new RatingAnswerType(), $entity, array(
            'action' => $this->generateUrl('ratinganswer_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing RatingAnswer entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MPSocialBundle:RatingAnswer')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find RatingAnswer entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('ratinganswer_edit', array('id' => $id)));
        }

        return $this->render('MPSocialBundle:RatingAnswer:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a RatingAnswer entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('MPSocialBundle:RatingAnswer')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find RatingAnswer entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('ratinganswer'));
    }

    /**
     * Creates a form to delete a RatingAnswer entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('ratinganswer_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
