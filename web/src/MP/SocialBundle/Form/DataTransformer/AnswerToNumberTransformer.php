<?php
namespace MP\SocialBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Doctrine\Common\Persistence\ObjectManager;
use MP\MPUserBundle\Entity\User;

class AnswerToNumberTransformer implements DataTransformerInterface
{
    /**
     * @var ObjectManager
     */
    private $om;

    /**
     * @param ObjectManager $om
     */
    public function __construct(ObjectManager $om)
    {
        $this->om = $om;
    }


    public function transform($answer)
    {
        if (null === $answer) {
            return "";
        }

        return $answer->getId();
    }

    /**
     * Transforms a string (number) to an object (issue).
     *
     * @param  string $number
     *
     * @return answer|null
     *
     * @throws TransformationFailedException if object (issue) is not found.
     */
    public function reverseTransform($number)
    {
        if (!$number) {
            return null;
        }

        $issue = $this->om
            ->getRepository('MPSocialBundle:Answer')
            ->find($number)
        ;

        if (null === $issue) {
            throw new TransformationFailedException(sprintf(
                'An answer with number "%s" does not exist!',
                $number
            ));
        }

        return $issue;
    }
}