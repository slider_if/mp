<?php

namespace MP\SocialBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use MP\UserBundle\Form\DataTransformer\UserToNumberTransformer;
use MP\UserBundle\Form\DataTransformer\CircleToNumberTransformer;

class QuestionType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $entityManager = $options['em'];
        $userTransformer = new UserToNumberTransformer($entityManager);
        $circleTransformer = new CircleToNumberTransformer($entityManager);
        $builder
            ->add('message')
            ->add( $builder->create('user', 'hidden')
                ->addModelTransformer($userTransformer))
            ->add( $builder->create('target', 'hidden')
                ->addModelTransformer($userTransformer))
            ->add( $builder->create('circle', 'hidden')
                ->addModelTransformer($circleTransformer))
//            ->add('target')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MP\SocialBundle\Entity\Question'
        ))->setRequired(array(
                'em',
            ))
            ->setAllowedTypes(array(
                'em' => 'Doctrine\Common\Persistence\ObjectManager',
            ));
//            ->setRequired(array(
//                'em',
//            ))
//            ->setAllowedTypes(array(
//                'em' => 'Doctrine\Common\Persistence\ObjectManager',
//            ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'mp_socialbundle_question';
    }
}
