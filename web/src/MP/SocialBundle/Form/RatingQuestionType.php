<?php

namespace MP\SocialBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use MP\SocialBundle\Form\DataTransformer\QuestionToNumberTransformer;
use MP\UserBundle\Form\DataTransformer\UserToNumberTransformer;
class RatingQuestionType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $entityManager = $options['em'];
        $questionTransformer = new QuestionToNumberTransformer($entityManager);
        $userTransformer = new UserToNumberTransformer($entityManager);
        $builder
            ->add('rating', 'choice', array(
                    'choices' => array(-5 => -5, -4=>-4, -3=>-3, -2=>-2, -1=>-1, 0=>0, 1=>1, 2=>2, 3=>3, 4=>4, 5=>5),
                    'required'  => true,
                    'data' => 0
                ))
            ->add($builder->create('question', 'hidden')
                ->addModelTransformer($questionTransformer))
            ->add( $builder->create('user', 'hidden')
                ->addModelTransformer($userTransformer))
        ;
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MP\SocialBundle\Entity\RatingQuestion'
        ))
            ->setRequired(array(
                'em',
            ))
            ->setAllowedTypes(array(
                'em' => 'Doctrine\Common\Persistence\ObjectManager',
            ));;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'mp_socialbundle_ratingquestion';
    }
}
