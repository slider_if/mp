<?php

namespace MP\SocialBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use MP\SocialBundle\Form\DataTransformer\QuestionToNumberTransformer;
use MP\UserBundle\Form\DataTransformer\UserToNumberTransformer;


class AnswerType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $entityManager = $options['em'];
        $questionTransformer = new QuestionToNumberTransformer($entityManager);
        $userTransformer = new UserToNumberTransformer($entityManager);

        $builder
            ->add('message')
            ->add($builder->create('question', 'hidden')
                ->addModelTransformer($questionTransformer))
            ->add( $builder->create('user', 'hidden')
                ->addModelTransformer($userTransformer))
//        ->add('save', 'submit',array('label'  => 'Due Date'))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MP\SocialBundle\Entity\Answer'
        ))
            ->setRequired(array(
                'em',
            ))
            ->setAllowedTypes(array(
                'em' => 'Doctrine\Common\Persistence\ObjectManager',
            ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'mp_socialbundle_answer';
    }
}
